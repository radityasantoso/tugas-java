package net.javaguides.todoapp.model;

import java.time.LocalDate;


/**
 * Todo.java
 * This is a model class represents a Todo entity
 * @author Ramesh Fadatare
 *
 */
public class Transaction {

    private int id;
    private String description;
    private LocalDate targetDate;
    private int amount;
    private int customerId;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(LocalDate targetDate) {
        this.targetDate = targetDate;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

//	protected Transaction() {
//
//	}

    public Transaction(int id,LocalDate targetDate, String description, int amount, int customerId) {
        super();
        this.id = id;
        this.targetDate = targetDate;
        this.description = description;
        this.amount = amount;
        this.customerId = customerId;
    }

    public Transaction(LocalDate targetDate, String description, int amount, int customerId) {
        super();
        this.targetDate = targetDate;
        this.description = description;
        this.amount = amount;
        this.customerId = customerId;
    }


}