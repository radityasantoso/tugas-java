package net.javaguides.todoapp.utils;

public class Session {
    private static int id;
    private static String nama;
    private static String username;
    private static String password;
    private static int pin;

    public static int getPin() {
        return pin;
    }

    public static void setPin(int pin) {
        Session.pin = pin;
    }



    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        Session.id = id;
    }

    public static String getNama() {
        return nama;
    }

    public static void setNama(String nama) {
        Session.nama = nama;
    }

    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        Session.username = username;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        Session.password = password;
    }



}
