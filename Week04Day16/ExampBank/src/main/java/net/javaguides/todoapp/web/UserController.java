package net.javaguides.todoapp.web;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.javaguides.todoapp.dao.TransactionDao;
import net.javaguides.todoapp.dao.UserDao;
import net.javaguides.todoapp.model.Transaction;
import net.javaguides.todoapp.model.User;
import net.javaguides.todoapp.utils.Session;

/**
 * @email Ramesh Fadatare
 */

@WebServlet("/register")
public class UserController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private UserDao userDao;
    private TransactionDao transactionDao;

    public void init() {
        userDao = new UserDao();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        register(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect("register/register.jsp");
    }

    private void register(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
        //mengambil value dari parameter
        String nama = request.getParameter("nama");
        String phone = request.getParameter("phone");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String pin = request.getParameter("pin");
        String saldo = request.getParameter("saldo");
        // validasi email regex
        boolean validUname = Pattern.matches("^[A-Za-z0-9+_.-]+@(.+)$", username);
        if (validUname == true) {
            // validasi password regex
            boolean validPass = Pattern.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$", password);
            if (validPass == true) {
                System.out.println("Anda Berhasil Login.....");
                User customer = new User();
                customer.setNama(nama);
                customer.setPhone(phone);
                customer.setUsername(username);
                customer.setPassword(password);
                customer.setPin(Integer.parseInt(pin));
                customer.setSaldo(Integer.parseInt(saldo));

                try {
                    int result = userDao.registerCustomer(customer);
                    if (result == 1) {
                        request.setAttribute("NOTIFICATION", "User Registered Successfully!");
                    }

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                Transaction trans = new Transaction(LocalDate.now(),"setoran awal",Integer.parseInt(saldo), Session.getId());
                transactionDao.insertTransaction(trans);

                RequestDispatcher dispatcher = request.getRequestDispatcher("register/register.jsp");
                dispatcher.forward(request, response);
            } else {
                //menampilkan error
                System.out.println("Password Salah");
                RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/Error.jsp");
                dispatcher.forward(request, response);

            }
        } else {
            //menampilkan error
            System.out.println("Username Salah");
            RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/Error.jsp");
            dispatcher.forward(request, response);


        }


    }
}
