package net.javaguides.todoapp.web;

import java.io.IOException;
import java.sql.*;
import java.time.LocalDate;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.javaguides.todoapp.dao.TodoDao;
import net.javaguides.todoapp.dao.TransactionDao;
import net.javaguides.todoapp.model.Transaction;
import net.javaguides.todoapp.utils.JDBCUtils;
import net.javaguides.todoapp.utils.Session;

/**
 * ControllerServlet.java This servlet acts as a page controller for the
 * application, handling all requests from the todo.
 * 
 * @email Ramesh Fadatare
 */

@WebServlet("/")
public class homeController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private TodoDao transactionDao;

    public void init() {
        transactionDao = new TransactionDao();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getServletPath();

        try {
            switch (action) {
                case "/new":
                    showNewForm(request, response);
                    break;
                case "/insert":
                    insertTransaction(request, response);
                    break;
                case "/delete":
                    deleteTodo(request, response);
                    break;
                case "/edit":
                    showEditForm(request, response);
                    break;
                case "/update":
                    updateTodo(request, response);
                    break;
                case "/home":
                    home(request, response);
                    break;
                case "/saldo":
                    saldo(request, response);
                    break;
                case "/mutasi":
                    mutasi(request, response);
                    break;
                case "/transfer":
                    transfer(request, response);
                    break;
                case "/saldoInformasi":
                    System.out.println("Button Pressed");
                    saldoInformasi(request,response);
                    break;
                case "/mutasiInformasi":
                    System.out.println("Button MutasiPressed");
                    mutasiList(request,response);
                    break;
                case "/transferInformasi":
                    System.out.println("Button transfer Pressed");
                    transferInformasi(request,response);
                    break;
                default:
                    RequestDispatcher dispatcher = request.getRequestDispatcher("login/login.jsp");
                    dispatcher.forward(request, response);
                    break;
            }
        } catch (SQLException ex) {
            throw new ServletException(ex);
        }
    }

    private void home(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
        System.out.println(request.getParameter("username"));

        try {
            Connection con = JDBCUtils.getConnection();
            String sql = "SELECT * FROM customers WHERE username = '"+request.getParameter("username")+"'";
            Statement stmt=con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Session.setId(rs.getInt("id"));
                Session.setNama(rs.getString("nama"));
                Session.setUsername(rs.getString("username"));
                Session.setPassword(rs.getString("password"));
            }
        }catch (Exception e){
            System.out.println(e);
        }
        System.out.println(Session.getNama());
        request.setAttribute("nama", Session.getNama());
        RequestDispatcher dispatcher = request.getRequestDispatcher("home/home.jsp");
        dispatcher.forward(request, response);
    }

    private void saldoInformasi(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
        String pin = request.getParameter("pin");
        System.out.println(pin);

        int totalSaldo = 0;
        boolean pinBenar = false;
        try {
            Connection con = JDBCUtils.getConnection();
            String sql = "SELECT *\n" +
                    "FROM users\n" +
                    "INNER JOIN transactions\n" +
                    "ON customers.id = transactions.customer_id\n" +
                    "WHERE username = '"+Session.getUsername()+"' AND pin = " +pin+" AND customer_id = '"+Session.getId()+"' ";
            Statement stmt=con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);


            while (rs.next()) {
                totalSaldo += rs.getInt("trans_amount");
                pinBenar = true;
            }

        }catch (Exception e){
            System.out.println(e);
        }

        if(pinBenar) {
            request.setAttribute("saldo",String.valueOf(totalSaldo));
            RequestDispatcher dispatcher = request.getRequestDispatcher("todo/saldoInformasi.jsp");
            dispatcher.forward(request, response);
        }else {
            RequestDispatcher dispatcher = request.getRequestDispatcher("todo/pinSalah.jsp");
            dispatcher.forward(request, response);
        }

    }

    private void saldo(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {

        RequestDispatcher dispatcher = request.getRequestDispatcher("todo/saldo.jsp");
        dispatcher.forward(request, response);
    }
    private void mutasi(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {

        RequestDispatcher dispatcher = request.getRequestDispatcher("todo/mutasi.jsp");
        dispatcher.forward(request, response);
    }
    private void transfer(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {

        RequestDispatcher dispatcher = request.getRequestDispatcher("todo/transfer.jsp");
        dispatcher.forward(request, response);
    }
    private void mutasiList(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
        String pin = request.getParameter("pin");
        List<Transaction> mutasiList = transactionDao.selectAllTodos();
        if(Integer.parseInt(pin) == Session.getPin()) {
            System.out.println("masuk pak eko");
            request.setAttribute("transactionList", mutasiList);
            RequestDispatcher dispatcher = request.getRequestDispatcher("todo/mutasiInformasi.jsp");
            dispatcher.forward(request, response);
        }else {
            RequestDispatcher dispatcher = request.getRequestDispatcher("todo/pinSalah.jsp");
            dispatcher.forward(request, response);
        }
    }

    private void transferInformasi(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
        // 1. Peraturan transfer
        // 1.1 jika saldo pengirim > jumlah transfer && pin dimasukin sesuai, maka transaksi sukses
        // - update saldo penerima (tambahin sesuai jumlah transfer)
        // - update saldo pengirim (kurangin sesuai jumlah transfer)
        // 1.2 jika saldo pengirim < jumlah transfer, maka saldo tidak cukup (rollback)
        // 1.3 jika pin yang dimasukan tidak sesuai, maka pin salah (rollback)
        String pin = request.getParameter("pin");
        String jumlahTransfer = request.getParameter("jumlahTransfer");
        String rekeningTujuan = request.getParameter("rekeningTujuan");
        System.out.println(pin);
        System.out.println(jumlahTransfer);
        System.out.println(rekeningTujuan);

        try {
            Connection con = JDBCUtils.getConnection();
            String sql = "SELECT * FROM customers";
            Statement stmt=con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);


            while (rs.next()) {
//				// harusnya kalo ada yang gagal 1 maka rollback, cuma berhubung waktu mau habis, belom di handle
                if(rs.getInt("id") == Integer.parseInt(rekeningTujuan)){
                    Transaction trans = new Transaction(LocalDate.now(),"Transfer Masuk",Integer.parseInt(jumlahTransfer),Integer.parseInt(rekeningTujuan));
                    String updateTotalSaldo = "UPDATE customers set saldo = saldo + ? WHERE id = ?";
                    PreparedStatement ps = con.prepareStatement(updateTotalSaldo);
                    ps.setInt(1,Integer.parseInt(jumlahTransfer));
                    ps.setInt(2, Integer.parseInt(rekeningTujuan));
                    ps.executeUpdate();
                    transactionDao.insertTransaction(trans);
                }else if(rs.getInt("id") == Session.getId()){
                    String updateTotalSaldo = "UPDATE customers set saldo = saldo - ? WHERE id = ?";
                    PreparedStatement ps = con.prepareStatement(updateTotalSaldo);
                    ps.setInt(1,Integer.parseInt(jumlahTransfer));
                    ps.setInt(2, Session.getId());
                    ps.executeUpdate();
                    Transaction trans = new Transaction(LocalDate.now(),"Transfer Keluar",-(Integer.parseInt(jumlahTransfer)),Session.getId());
                    transactionDao.insertTransaction(trans);
                }
            }

        }catch (Exception e){
            System.out.println(e);
        }





    }

    private void showNewForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("todo/todo-form.jsp");
        dispatcher.forward(request, response);
    }

    private void showEditForm(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Transaction existingTodo = transactionDao.selectTodo(id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("todo/todo-form.jsp");
        request.setAttribute("todo", existingTodo);
        dispatcher.forward(request, response);

    }

    private void insertTransaction(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {

        String title = request.getParameter("title");
        String username = request.getParameter("username");
        String description = request.getParameter("description");

		/*DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-mm-dd");
		LocalDate targetDate = LocalDate.parse(request.getParameter("targetDate"),df);*/

        boolean isDone = Boolean.valueOf(request.getParameter("isDone"));
//		Transaction newTodo = new Transaction(title, username, description, LocalDate.now(), isDone);
//		todoDAO.insertTransaction(newTodo);
        response.sendRedirect("list");
    }

    private void updateTodo(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));

        String title = request.getParameter("title");
        String username = request.getParameter("username");
        String description = request.getParameter("description");
        //DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-mm-dd");
        LocalDate targetDate = LocalDate.parse(request.getParameter("targetDate"));

        boolean isDone = Boolean.valueOf(request.getParameter("isDone"));
//		Transaction updateTodo = new Transaction(id, title, username, description, targetDate, isDone);

//		todoDAO.updateTodo(updateTodo);

        response.sendRedirect("list");
    }

    private void deleteTodo(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        transactionDao.deleteTodo(id);
        response.sendRedirect("list");
    }
}