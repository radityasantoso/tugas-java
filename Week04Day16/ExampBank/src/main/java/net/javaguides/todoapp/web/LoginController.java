package net.javaguides.todoapp.web;

import java.io.IOException;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.javaguides.todoapp.dao.LoginDao;
import net.javaguides.todoapp.model.LoginBean;
import net.javaguides.todoapp.utils.Session;

/**
 * @email Ramesh Fadatare
 */

@WebServlet("/login")
public class LoginController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private LoginDao loginDao;

    public void init() {
        loginDao = new LoginDao();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.sendRedirect("login/login.jsp");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        authenticate(request, response);
    }

    private void authenticate(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        //mengambil value dari parameter
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        LoginBean loginBean = new LoginBean();
        loginBean.setUsername(username);
        loginBean.setPassword(password);

        try {
            // validasi email regex
            boolean validUname = Pattern.matches("^[A-Za-z0-9+_.-]+@(.+)$", username);
            if (validUname == true) {
                //validasi password
                boolean validPass = Pattern.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$", password);
                if (validPass == true) {
                    System.out.println("Anda Berhasil Login.....");
                    System.out.println("tes");
                    //validasi email password berdasarkan database
                    if (loginDao.validate(loginBean)) {
                        request.setAttribute("nama", Session.getNama());
                        RequestDispatcher dispatcher = request.getRequestDispatcher("home/home.jsp");
                        dispatcher.forward(request, response);
                    } else {
                        // back ke logn
                        HttpSession session = request.getSession();
                        System.out.println("tes2");
                        session.setAttribute("user", username);
                        response.sendRedirect("login.jsp");

                    }

                } else {
                    //menampilkan eror
                    System.out.println("Password Salah");
                    RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/Error.jsp");
                    dispatcher.forward(request, response);

                }
            } else {
                //menampilkan error
                System.out.println("Username Salah");
                RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/Error.jsp");
                dispatcher.forward(request, response);
            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}