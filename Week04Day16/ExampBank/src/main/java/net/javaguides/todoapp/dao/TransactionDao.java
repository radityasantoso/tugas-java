package net.javaguides.todoapp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import net.javaguides.todoapp.model.Transaction;
import net.javaguides.todoapp.utils.JDBCUtils;
import net.javaguides.todoapp.utils.Session;

/**
 * This DAO class provides CRUD database operations for the table todos in the
 * database.
 * 
 * @author Ramesh Fadatare
 *
 */

public class TransactionDao implements TodoDao {

    private static final String INSERT_TRANSACTIONS_SQL = "INSERT INTO transactions"
            + "  (trans_date, trans_desc, trans_amount, customer_id) VALUES "
            + " (?, ?, ?, ?);";

    private static final String INSERT_TODOS_SQL = "INSERT INTO todos"
            + "  (title, username, description, target_date,  is_done) VALUES " + " (?, ?, ?, ?, ?);";

    private static final String SELECT_TODO_BY_ID = "select id,title,username,description,target_date,is_done from todos where id =?";
    private static final String SELECT_ALL_TRANSACTION = "select * from transactions where customer_id = ?";

    private static final String SELECT_ALL_TRANSCTION_BY_CUSTOMERID = "SELECT *\n" +
            "FROM users\n" +
            "INNER JOIN transactions\n" +
            "ON customers.id = transactions.customer_id\n" +
            "WHERE username = ? AND pin = ? AND customer_id = ? ";
    private static final String DELETE_TODO_BY_ID = "delete from todos where id = ?;";
    private static final String UPDATE_TODO = "update todos set title = ?, username= ?, description =?, target_date =?, is_done = ? where id = ?;";

    public TransactionDao() {
    }

    @Override
    public void insertTransaction(Transaction trans) throws SQLException {
        System.out.println(INSERT_TRANSACTIONS_SQL);
        // try-with-resource statement will auto close the connection.
        try (Connection connection = JDBCUtils.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_TRANSACTIONS_SQL)) {
            preparedStatement.setDate(1, JDBCUtils.getSQLDate(trans.getTargetDate()));
            preparedStatement.setString(2, trans.getDescription());
            preparedStatement.setInt(3, trans.getAmount());
            preparedStatement.setInt(4, trans.getCustomerId());
            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException exception) {
            JDBCUtils.printSQLException(exception);
        }
    }

    @Override
    public Transaction selectTodo(long todoId) {
        Transaction todo = null;
        // Step 1: Establishing a Connection
        try (Connection connection = JDBCUtils.getConnection();
             // Step 2:Create a statement using connection object
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_TODO_BY_ID);) {
            preparedStatement.setLong(1, todoId);
            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();

            // Step 4: Process the ResultSet object.
            while (rs.next()) {
                long id = rs.getLong("id");
                LocalDate targetDate = rs.getDate("trans_date").toLocalDate();
                int title = rs.getInt("title");
                String username = rs.getString("username");
                String description = rs.getString("description");

                boolean isDone = rs.getBoolean("is_done");
//				todo = new Transaction(id, title, username, description, targetDate, isDone);
            }
        } catch (SQLException exception) {
            JDBCUtils.printSQLException(exception);
        }
        return todo;
    }

    @Override
    public List<Transaction> selectAllTodos() {

        // using try-with-resources to avoid closing resources (boiler plate code)
        List<Transaction> todos = new ArrayList<>();

        // Step 1: Establishing a Connection
        try (Connection connection = JDBCUtils.getConnection();

             // Step 2:Create a statement using connection object
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_TRANSCTION_BY_CUSTOMERID);) {
            preparedStatement.setString(1, Session.getUsername());
            preparedStatement.setInt(2, Session.getPin());
            preparedStatement.setInt(3, Session.getId());
            System.out.println(preparedStatement);

            // Step 3: Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();

            // Step 4: Process the ResultSet object.
            while (rs.next()) {
                int id = rs.getInt("trans_id");
                LocalDate date = rs.getDate("trans_date").toLocalDate();
                String description = rs.getString("trans_desc");
                int amount = rs.getInt("trans_amount");
                int customerId = rs.getInt("customer_id");

                todos.add(new Transaction(id, date, description, amount,customerId));
            }
        } catch (SQLException exception) {
            System.out.println("Transaction Dao line 111");
            JDBCUtils.printSQLException(exception);
        }
        return todos;
    }

    @Override
    public boolean deleteTodo(int id) throws SQLException {
        boolean rowDeleted;
        try (Connection connection = JDBCUtils.getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_TODO_BY_ID);) {
            statement.setInt(1, id);
            rowDeleted = statement.executeUpdate() > 0;
        }
        return rowDeleted;
    }

    @Override
    public boolean updateTodo(Transaction todo) throws SQLException {
        boolean rowUpdated;
        try (Connection connection = JDBCUtils.getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_TODO);) {
//			statement.setString(1, todo.getTitle());
//			statement.setString(2, todo.getUsername());
//			statement.setString(3, todo.getDescription());
//			statement.setDate(4, JDBCUtils.getSQLDate(todo.getTargetDate()));
//			statement.setBoolean(5, todo.getStatus());
//			statement.setLong(6, todo.getId());
            rowUpdated = statement.executeUpdate() > 0;
        }
        return rowUpdated;
    }
}