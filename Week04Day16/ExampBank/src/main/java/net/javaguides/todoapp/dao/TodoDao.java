package net.javaguides.todoapp.dao;

import java.sql.SQLException;
import java.util.List;
import net.javaguides.todoapp.model.Transaction;

public interface TodoDao {

    void insertTransaction(Transaction todo) throws SQLException;

    Transaction selectTodo(long todoId);

    List<Transaction> selectAllTodos();

    boolean deleteTodo(int id) throws SQLException;

    boolean updateTodo(Transaction todo) throws SQLException;

}
