package net.javaguides.todoapp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import net.javaguides.todoapp.model.User;
import net.javaguides.todoapp.utils.JDBCUtils;

public class UserDao {

    public int registerCustomer(User customer) throws ClassNotFoundException {
        String INSERT_USERS_SQL = "INSERT INTO users" +
                "  (nama, phone, username, password, pin, saldo) VALUES " +
                " (?, ?, ?, ?,?,?);";

        int result = 0;
        try (Connection connection = JDBCUtils.getConnection();
             // Step 2:Create a statement using connection object
             PreparedStatement preparedStatement = 	connection.prepareStatement(INSERT_USERS_SQL)) {
            preparedStatement.setString(1, customer.getNama());
            preparedStatement.setString(2, customer.getPhone());
            preparedStatement.setString(3, customer.getUsername());
            preparedStatement.setString(4, customer.getPassword());
            preparedStatement.setInt(5, customer.getPin());
            preparedStatement.setInt(6, customer.getSaldo());

            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            result = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            // process sql exception
            JDBCUtils.printSQLException(e);
        }
        return result;
    }
}
