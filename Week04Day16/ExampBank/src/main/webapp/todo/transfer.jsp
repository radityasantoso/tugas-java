<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Transfer</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>

</head>
<body>
	<header>
		<nav class="navbar navbar-expand-md navbar-dark"
			style="background-color: tomato">
			<div>
				<a href="http://localhost:8080/banking-application/home" class="navbar-brand"> Banking
					App</a>
			</div>

			<ul class="navbar-nav">
				<li><a href="<%=request.getContextPath()%>/saldo"
					class="nav-link">Informasi Saldo</a></li>
			</ul>

			<ul class="navbar-nav">
                <li><a href="<%=request.getContextPath()%>/mutasi"
                    class="nav-link">Mutasi Rekening</a></li>
            </ul>

            <ul class="navbar-nav">
                <li><a href="<%=request.getContextPath()%>/transfer"
                    class="nav-link">Transfer</a></li>
            </ul>
            <ul class="navbar-nav">
                <li><a href="<%=request.getContextPath()%>/pulsa"
                    class="nav-link">Pembelian Pulsa</a></li>
            </ul>

			<ul class="navbar-nav navbar-collapse justify-content-end">
				<li><a href="<%=request.getContextPath()%>/logout"
					class="nav-link">Logout</a></li>
			</ul>
		</nav>
	</header>

	<div class="row">
		<!-- <div class="alert alert-success" *ngIf='message'>{{message}}</div> -->

		<div class="container">
			<h3 class="text-center">Transfer</h3>
            <hr>
            <form action="<%= request.getContextPath() %>/transferInformasi" method="post">
            <div class="container text-left">
                <div class="mb-1"><p class="mb-2">Nomor Rekening Tujuan:</p> <input type="text" name="rekeningTujuan"></div>
                <div class="mb-1"><p class="mb-2">Jumlah Transfer:</p> <input type="number" name="jumlahTransfer"></div>
                <div class="mb-4"><p class="mb-2">PIN:</p><input type="text" name="pin"></div>
                <input type="submit" class="btn btn-success" value="submit">
            </div>
            </form>


		</div>
	</div>

	<jsp:include page="../common/footer.jsp"></jsp:include>
</body>
</html>
