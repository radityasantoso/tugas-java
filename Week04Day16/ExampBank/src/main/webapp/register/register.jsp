<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
 pageEncoding="ISO-8859-1"%>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="ISO-8859-1">
        <title>Insert title here</title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>

    </head>

    <body>
        <jsp:include page="../common/header.jsp"></jsp:include>
        <div class="container">

            <h2>User Register Form</h2>
            <div class="col-md-6 col-md-offset-3">
                <div class="alert alert-success center" role="alert">
                    <p>${NOTIFICATION}</p>
                </div>

                <form action="<%=request.getContextPath()%>/register" method="post">

                    <div class="form-group">
                        <label for="nama">First Name:</label> <input type="text" class="form-control" id="nama" placeholder="Name" name="nama" required>
                    </div>

                    <div class="form-group">
                        <label for="phone">Phone Number:</label> <input type="text" class="form-control" id="phone" placeholder="Phone Number" name="phone" required>
                    </div>

                    <div class="form-group">
                        <label for="uname">User Name:</label> <input type="email" class="form-control" id="username" placeholder="User Name" name="username"
                         title="Invalid email address" required>
                    </div>

                    <div class="form-group">
                        <label for="password">Password:</label> <input type="password" class="form-control" id="password" placeholder="Password" name="password"

                        title="At least 8 chars and Contains at least one digit, one lower alpha char and one upper alpha char, one char within a set of special chars. Does not contain space, tab, etc" required>
                    </div>
                    <div class="form-group">
                        <label for="pin">Pin Code Number:</label> <input type="password" class="pincode" id="pin" placeholder="Pin" name="pin" maxlength="4"required>
                    </div>
                    <div class="form-group">
                        <label for="saldo">Saldo:</label> <input type="number" class="form-control" id="saldo" placeholder="Saldo Awal" name="saldo" min="500000"required>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>

                </form>
            </div>
        </div>
        <jsp:include page="../common/footer.jsp"></jsp:include>
    </body>

    </html>