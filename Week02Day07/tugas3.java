package comp.week2day7.tugas3;
class Parent{
    //mendeklarasikan dan menginisialisasi variabel
    String name = "Mr. USD";
    int money = 2000000;
    //create method
    public void Home(){
        System.out.println("Parent's Home");
    }
    public void Car(){
        System.out.println("Parent's Car");
    }

}
//membuat class child dari pewarisan parent
class Child extends Parent{
    //mendeklarasi dan menginisialisasi variabel
    String name="Tom";
    int money=200;
    //create method car
    public void Car(){
        //call method pewarisan dari parent
        super.Home();
        super.Car();
        System.out.println("Child Car");

    }
    //create method
    public void parentinfo(){
        System.out.println("Parent's Name :"+super.name);//call variabel pewarisan dari class parent
        System.out.println("Parent's Money :"+super.money);//call variabel pewarisan dari class parent
    }
    public void childtinfo(){
        System.out.println("Child Name :"+name);
        System.out.println("Child Money :"+money);
    }

}

public class tugas3 {
    public static void main(String[] args) {
        //create object class child
        Child vwc= new Child();
        //Call method
        vwc.Car();
        System.out.println("=============");
        vwc.parentinfo();
        System.out.println("=============");
        vwc.childtinfo();
    }



}
