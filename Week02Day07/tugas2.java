package comp.week2day7.assignment2;
class Parent{
    //create methode home

    public void Home(){
        System.out.println("Parent Home");
    }
    //create methode car
    public void Car(){
        System.out.println("Parent Car");
    }

}
class Child extends Parent{

    public void Car(){
        //Pewarisan method dari class parent
        super.Home();
        super.Car();
        System.out.println("Child Car");

    }

}

public class tugas2 {
    public static void main(String[] args) {
        //create object class child
        Child vwc= new Child();
        //call object class child and method car
        vwc.Car();
    }



}
