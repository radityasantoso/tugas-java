package comp.week2day7.assignment4;

import java.util.*;

//create class abstract
abstract class Worker{
    int id;
    String nama;
    int absensi;
    //create method abstract

    abstract void tambahAbsensi();
}
//membuat class pewarisan dari class abstract worker
class Staff extends Worker{
    String jabatan;

    public Staff() {

    }


    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }
    //create constructor staff
    Staff(int id, String nama, String jabatan){
        this.id = id;
        this.nama = nama;
        this.jabatan = jabatan;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getAbsensi() {
        return absensi;
    }

    public void setAbsensi(int absensi) {
        this.absensi = absensi;
    }
    //override method abstract from abstract class worker
    @Override
    void tambahAbsensi() {
        //callculated
        this.absensi = this.absensi + 1;
    }
    //create object map
    Map map = new HashMap();
    Scanner input = new Scanner(System.in);
    //create method add data staff
    public void addStaff(){

        System.out.print("Input ID : ");
        int id = this.input.nextInt();
        System.out.print("Input Nama : ");
        String nama = this.input.next();
        System.out.print("Input Jabatan : ");
        String jabatan = this.input.next();

        Staff st = new Staff(id, nama, jabatan);
        map.put(id,st);//set object st to map

    }
    //create method view data staff
    public void viewStaff(){

        Set set=map.entrySet();//Converting to Set so that we can traverse
        Iterator itr=set.iterator();
        System.out.println("Id\t Nama\t Jabatan\tAbsensi/Hari");

        while(itr.hasNext()){

            //Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry=(Map.Entry)itr.next();
            //Convert to Staff
            Staff st = (Staff) entry.getValue();
            //Print data
            System.out.println(entry.getKey()+"\t"+st.getNama()+"\t\t"+st.getJabatan()+"\t"+st.getAbsensi());
        }
    }
    //membuat method tambah absen
    public void tambahAbsen() {

        System.out.print("Input ID : ");
        int id = this.input.nextInt();

        Set set=map.entrySet();//Converting to Set so that we can traverse
        Iterator itr=set.iterator();

        System.out.println("ID\tNama\tJabatan");

        while(itr.hasNext()){

            //Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry=(Map.Entry)itr.next();
            //Convert to Staff
            Staff st = (Staff) entry.getValue();

            if(id == st.getId()){
                System.out.println(entry.getKey()+"\t"+st.getNama()+"\t"+st.getJabatan());
                st.tambahAbsensi();
            }

        }
        System.out.println("Success.......");
    }

}
public class Tugas4 {
    public static void main(String[] args) {
        //Create object Staff
        Staff sc = new Staff();

        Scanner inputI = new Scanner(System.in);
        int w = 1, loop = 2;
        while (w < loop) {
            System.out.println("MENU ");
            System.out.println("1. Tambah Nama Staff");
            System.out.println("2. Tampilkan Laporan Staff ");
            System.out.println("3. EXIT ");
            System.out.println("");
            System.out.print("Input Menu : ");
            System.out.print("");

            //mendeklarasikan switch menu
            int menu = inputI.nextInt();
            switch (menu) {
                case 1:
                    //call object method staff
                    sc.addStaff();
                    break;
                case 2:
                    //call object method staff
                    sc.tambahAbsen();
                    break;
                case 3:
                    //call object method staff
                    sc.viewStaff();
                    break;
                default:
                    System.out.println("== Anda telah Exit ==");
                    //mengubah variabel parameter untuk berhenti perulangan input menu
                    w = 10;
                    inputI.close();
                    break;

            }
        }
    }
}
