package comp.week2day7.assignment1;

import comp.week2day6.fix2.controller.StaffController;

import java.util.*;
//membuat kelas worker

class Worker{
    int id;
    String nama;


}
//pewarisan class staff dari class worker
class Staff extends Worker{
    String jabatan;

    public Staff(int id,String nama,String jabatan) {
        this.id=id;
        this.nama=nama;
        this.jabatan=jabatan;
    }

    public Staff() {

    }


    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
    Map map=new HashMap();

    
    public void inputData(){
        Scanner inputS= new Scanner(System.in);
        Scanner inputI= new Scanner(System.in);

        //Adding elements to map
            System.out.print("Input Id Staff : ");
            id = inputI.nextInt();
            System.out.print("Input Nama Staff : ");
            nama = inputS.nextLine();
            System.out.print("Input Jabatan Staff : ");
            jabatan = inputS.nextLine();
            Staff st= new Staff(id,nama,jabatan);
            map.put(id, st);
        }
        //mengkonversi untuk mendapatkan key dan value kemudian di tampilkan



        //mengkonversi untuk mendapatkan key dan value kemudian di tampilkan

    public void viewStaff(){
        Set set=map.entrySet();//Converting to Set so that we can traverse
        Iterator itr=set.iterator();
        System.out.println("Id\t Nama\t Jabatan");

        while(itr.hasNext()){

            //Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry=(Map.Entry)itr.next();
            //Convert to Staff
            Staff st = (Staff) entry.getValue();
            //Print data
            System.out.println(entry.getKey()+"\t"+st.getNama()+"\t"+st.getJabatan());
        }

    }
}

public class tugas1 {
    public static void main(String[] args) {
        //Create object Staff
        Staff sc = new Staff();
        Scanner inputI = new Scanner(System.in);
        int w = 1, loop = 2;
        while (w < loop) {
            System.out.println("MENU ");
            System.out.println("1. Tambah Nama Staff");
            System.out.println("2. Tampilkan Laporan Staff ");
            System.out.println("3. EXIT ");
            System.out.println("");
            System.out.print("Input Menu : ");
            System.out.print("");

            //mendeklarasikan switch menu
            int menu = inputI.nextInt();
            switch (menu) {
                case 1:
                    //call object method staff
                    sc.inputData();
                    break;
                case 2:
                    //call object method staff
                    sc.viewStaff();
                    break;
                default:
                    System.out.println("== Anda telah Exit ==");
                    //mengubah variabel parameter untuk berhenti perulangan input menu
                    w = 10;
                    inputI.close();
                    break;

            }
        }
    }
}