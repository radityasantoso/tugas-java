package comp.week2day8;


import java.util.*;
//membuat interface worker
interface Worker{
    //calll method di class staff implement worker
    void tambahAbsensi(int absensi);
}
//membuat class implement class interface
class Staff implements Worker{
    int id;
    String nama;
    int gapok;
    int absensi=20;
    int tunjMakan;
    int totGaji;
    String jabatan;

    public int getTotGaji() {
        return totGaji;
    }

    public void setTotGaji(int totGaji) {
        this.totGaji = totGaji;
    }

    //create constructor
    public Staff(int id, String nama, int gapok, String jabatan) {
        this.id = id;
        this.nama = nama;
        this.gapok = gapok;
        this.jabatan = jabatan;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getGapok() {
        return gapok;
    }

    public void setGapok(int gapok) {
        this.gapok = gapok;
    }

    public int getTunjMakan() {
        return tunjMakan;
    }

    public void setTunjMakan(int tunjMakan) {
        this.tunjMakan = tunjMakan;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public int getAbsensi() {
        return absensi;
    }

    public void setAbsensi(int absensi) {
        this.absensi = absensi;
    }

    @Override
    public void tambahAbsensi(int absensi) {
        //callculated
        this.absensi = this.absensi + 1;
    }

    public void tambahAbsensi() {
    }
    public void hitungTunj() {
        tunjMakan = absensi * 20000;
    }
    public void sumGaji(){
        totGaji = gapok + tunjMakan;
    }

}
public class Assignment1 {
    Map m = new HashMap();
    Scanner inputI = new Scanner(System.in);
    Scanner inputS = new Scanner(System.in);


    //method input data staff
    public void addStaff(){

        System.out.print("Input ID : ");
        int id = this.inputI.nextInt();
        System.out.print("Input Nama : ");
        String nama = this.inputS.nextLine();
        System.out.print("Input Gaji Pokok : ");
        int gapok = this.inputI.nextInt();
        System.out.print("Input Jabatan : ");
        String jabatan = this.inputS.nextLine();


        Staff st = new Staff(id, nama, gapok, jabatan);

        m.put(id,st);

    }
    //methode tambah absen
    public void tambahAbsen() {

        System.out.print("Input ID : ");
        int id = this.inputI.nextInt();

        Set set=m.entrySet();//Converting to Set so that we can traverse
        Iterator itr=set.iterator();

        System.out.println("ID\tNama\tJabatan");

        while(itr.hasNext()){

            //Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry=(Map.Entry)itr.next();
            //Convert to Staff
            Staff st = (Staff) entry.getValue();

            if(id == st.getId()){
                System.out.println(entry.getKey()+"\t"+st.getNama()+"\t"+st.getJabatan());
                st.tambahAbsensi();
            }

        }
    }
    //method hitung tunjangan
    public void hitungTunjangan(){
        System.out.print("Input ID : ");
        int id = this.inputI.nextInt();

        Set set=m.entrySet();//Converting to Set so that we can traverse
        Iterator itr=set.iterator();

        System.out.println("ID\tNama\tJabatan");

        while(itr.hasNext()){

            //Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry=(Map.Entry)itr.next();
            //Convert to Staff
            Staff st = (Staff) entry.getValue();

            if(id == st.getId()){
                st.hitungTunj();
            }
        }
    }
    //methode menampilkan data
    public void viewStaff(){

        Set set=m.entrySet();//Converting to Set so that we can traverse
        Iterator itr=set.iterator();
        System.out.println("Id\t Nama\t Jabatan\tAbsensi/Hari");

        while(itr.hasNext()){

            //Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry=(Map.Entry)itr.next();
            //Convert to Staff
            Staff st = (Staff) entry.getValue();
            //Print data
            System.out.println(entry.getKey()+"\t"+st.getNama()+"\t\t"+st.getGapok()+"\t"+st.getJabatan()
                    +"\t"+st.getAbsensi()+"\t"+st.getTotGaji());
        }
    }


    public static void main(String args[]) {

        Assignment1 obj = new Assignment1();
        Scanner inputI = new Scanner(System.in);

        int pil=0;

        do {
            System.out.println("MENU");
            System.out.println("0. Buat Dummy Data");
            System.out.println("1. Buat Staff");
            System.out.println("2. Tampilkan Data Staff");

            System.out.println("99. EXIT");

            System.out.print("Input nomor : ");
            pil = obj.inputI.nextInt();

            switch (pil) {
                // performs addition between numbers
                case 1:
                    //call method addstaff
                    obj.addStaff();
                    break;

                case 2:
                    //call methode tambah absen
                    obj.tambahAbsen();
                    break;

                case 3:
                    //call method hitung tunjangan
                    obj.hitungTunjangan();
                    break;
                case 4:
                    //call metod laporan
                    obj.viewStaff();
                    break;
            }
            System.out.println();

        } while(pil != 99);

        // closing the scanner object
        obj.inputI.close();

    }

}
