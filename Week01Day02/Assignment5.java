import java.util.Scanner;
public class LatihanWhile{

     public static void main(String []args){
      Scanner input = new Scanner(System.in);
      
      
      //mendeklarasikan dan menginisialisasi variabel i dan menu
      int i=1, menu=4;
        //pengkondisian untuk perulangan menu
        while(i<menu){
            System.out.println("Menu");
            System.out.println("1. Cetak Menu 1");
            System.out.println("2. Cetak Menu 2");
            System.out.println("3. Cetak Menu 3");
            System.out.println("4. Exit");
            //input data menu
            System.out.println("Pilih Menu :");
            i = input.nextInt();
            //cetak data yang telah di input
            System.out.println("Menu "+ i);
        }
      //keluar dari pengkondisian 
      System.out.println("Anda Telah Exit");
     }
}