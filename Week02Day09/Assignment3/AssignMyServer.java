package comp.assignment;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;
import java.util.Scanner;

public class AssignMyServer {

    //Login

    //Read Properties File
    int port = 0;
    String ip = "";

    public void getPropValues() throws IOException {
        System.out.println("Read Config");
        InputStream inputStream = null;

        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }

            // get the property value and print it out
            this.port = Integer.parseInt(prop.getProperty("PORT"));
            this.ip = prop.getProperty("IP");

            System.out.println("PORT = "+port);
            System.out.println("IP = "+ip);

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            inputStream.close();
        }

    }

    ServerSocket ss;
    Socket s;
    DataInputStream dis;
    DataOutputStream dout;
    Scanner input;
    String serverMsg;

    public void serverUp(){
        System.out.println("Binding Port");

        try{
            this.ss=new ServerSocket(this.port);
            this.s=ss.accept();//establishes connection

            dis=new DataInputStream(s.getInputStream());
            dout=new DataOutputStream(s.getOutputStream());

        }catch(Exception e){System.out.println(e);}
    }

    public void serverDown() throws IOException {
        this.ss.close();
    }

    public void serverRead() throws IOException {
        System.out.println("Read Data");

        input  = new Scanner(System.in);
        String msg = "";

        String  str=(String)dis.readUTF();

        System.out.println("Message from Client = "+str);

        String [] parsedStrGet = str.trim().split("\n");
        for (int i = 0; i < parsedStrGet.length; i++) {
            String [] parsedStrGet2 = parsedStrGet[i].trim().split(",");

            System.out.println("Nama :"+parsedStrGet2[0]);
            System.out.println("Jabatan :"+parsedStrGet2[1]);
            System.out.println("Gaji Pokok :"+parsedStrGet2[2]);
            System.out.println("");


        }

        dout.close();

    }


    public static void main(String[] args) throws IOException {

        AssignMyServer obj = new AssignMyServer();

        //Login

        //Baca Prop
        obj.getPropValues();

        //Server Make Connection
        obj.serverUp();

        obj.serverRead();

        obj.serverDown();
    }

}
