package comp.assignment2;

import comp.properties.CrunchifyGetPropertyValues;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.Pattern;

public class AssignMyClient {
    Scanner inputS = new Scanner(System.in);
    Scanner inputI = new Scanner(System.in);
    int port = 0;
    String ip = "";
    InputStream inputStream = null;
    public void getPropValues() throws IOException {
        System.out.println("Read Config");


        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }

            // get the property value and print it out
            this.port = Integer.parseInt(prop.getProperty("PORT"));
            this.ip = prop.getProperty("IP");

            System.out.println("PORT = "+port);
            System.out.println("IP = "+ip);

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            inputStream.close();
        }

    }
    ServerSocket ss;
    Socket s;
    DataInputStream dis;
    DataOutputStream dout;
    Scanner input;
    String serverMsg;

    public void serverUp(){
        System.out.println("Binding Port");

        try{

            CrunchifyGetPropertyValues config = new CrunchifyGetPropertyValues();
            int port = Integer.parseInt(config.getPropValues("PORT"));
            String ip = config.getPropValues("IP");
            this.s=new Socket(ip,port);
            this.s=ss.accept();//establishes connection

            dis=new DataInputStream(s.getInputStream());
            dout=new DataOutputStream(s.getOutputStream());

        }catch(Exception e){System.out.println(e);}
    }
    public void serverDown() throws IOException {
        this.ss.close();
    }
    public void Sending() throws IOException {
        try {
            DataOutputStream dout = new DataOutputStream(s.getOutputStream());
            dout.writeUTF(data);
            dout.flush();
            DataInputStream dis = new DataInputStream(s.getInputStream());
            String str = (String) dis.readUTF();
            System.out.println("message= " + str);
            dout.close();
        }catch(Exception e) {
            System.out.println(e);
        }finally {

            //s.close();
        }
    }
    public void SendExit(){
        try {
            String str = "";
            while (!str.equalsIgnoreCase("exit")) {
                DataOutputStream dout = new DataOutputStream(s.getOutputStream());
                dout.writeUTF("EXIT");
                dout.flush();
                DataInputStream dis = new DataInputStream(s.getInputStream());

                str = (String) dis.readUTF();
                System.out.println("message= " + str);
                if(str.equalsIgnoreCase("exit"))break;


                dout.close();
                s.close();
            }
            }catch(Exception e){
                System.out.println(e);
            }

    }

    String data ="";

    void fileReader() {
        FileReader fr;
        BufferedReader br;

        try {
            fr = new FileReader("C:\\workspace\\day9.txt");
            br = new BufferedReader(fr);
            int i;
            while ((i = br.read()) != -1) {
                data = data + (char) i;
            }
            System.out.println(data);
            System.out.println("Success......");
            br.close();
            fr.close();
        } catch (IOException e) {
            System.out.println(e);
        }
    }
    void login() {
        int w = 1, loop = 2;
        while (w < loop) {
            System.out.print("Username : ");
            String unameSU = inputS.next();
            boolean validUname = Pattern.matches("^[A-Za-z0-9+_.-]+@(.+)$", unameSU);
            if (validUname == true) {
                System.out.print("Password : ");
                String passSU = inputS.next();
                boolean validPass = Pattern.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$", passSU);
                if (validPass == true) {
                    System.out.println("Anda Berhasil Login.....");
                    w = 3;
                } else {
                    System.out.println("Password Salah");
                    w = 1;
                }
            } else {
                System.out.println("Username Salah");
                w = 1;
            }

        }
    }
    public static void main(String[] args) throws IOException {
        Scanner inputS = new Scanner(System.in);
        Scanner inputI = new Scanner(System.in);
        AssignMyClient obj = new AssignMyClient();
        obj.login();
        obj.fileReader();
        int w=1, loop=2;
        while(w<loop) {
            System.out.println("MENU ");
            System.out.println("1. Connect Socket ");
            System.out.println("2. Send Data to Server ");
            System.out.println("3. EXIT ");
            System.out.println("");
            System.out.print("Input Menu : ");
            System.out.print("");
            //mendeklarasikan switch menu
            int menu = inputI.nextInt();
            switch (menu) {
                case 1:
                    obj.getPropValues();
                    obj.serverUp();
                    System.out.println("Connected...");
                    break;
                case 2:
                    obj.fileReader();
                    obj.Sending();
                    break;
                case 3:
                    obj.SendExit();
                    break;
                default :
                    System.out.println("== Anda telah Exit ==");
                    w= 10;
                    obj.serverDown();
                    inputI.close();
                    inputS.close();
                    break;

            }
        }

    }
}

