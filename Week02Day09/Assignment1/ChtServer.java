package comp.assignment1;

import comp.properties.CrunchifyGetPropertyValues;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class ChtServer {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        ServerSocket ss = null;
        DataInputStream din = null;
        DataOutputStream dout = null;
        CrunchifyGetPropertyValues config = new CrunchifyGetPropertyValues();
        try {
            // Load config.properties
            int port = Integer.parseInt(config.getPropValues("PORT"));
            ss = new ServerSocket(port);
            System.out.println("Server menunggu request dari Client");

            Socket socket = ss.accept();
            din = new DataInputStream(socket.getInputStream());

            OutputStream outputStream = socket.getOutputStream();
            dout = new DataOutputStream(outputStream);

            String chtClient = "";
            String chtServer = "";
            //pengkondisian perulangan chat
            while (!chtClient.equalsIgnoreCase("exit")) {
                //membaca chat client
                chtClient = din.readUTF();
                System.out.println("Pesan dari Client: " + chtClient);
                //mengirim pesan ke client
                chtServer = input.nextLine();
                dout.writeUTF(chtServer);
                dout.flush();
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            try {
                input.close();
                din.close();
                dout.close();
                ss.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
