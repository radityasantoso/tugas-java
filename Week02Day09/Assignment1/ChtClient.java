package comp.assignment1;

import comp.properties.CrunchifyGetPropertyValues;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class ChtClient {
    public static void main(String[] args) {
        Socket socket = null;
        DataInputStream din = null;
        DataOutputStream dout = null;
        Scanner input = new Scanner(System.in);
        // menghubungkan config.properties port & ipnya
        CrunchifyGetPropertyValues config = new CrunchifyGetPropertyValues();
        try {

            int port = Integer.parseInt(config.getPropValues("PORT"));
            String ip = config.getPropValues("IP");
            socket = new Socket(ip, port);
            //membuat data input stream
            din = new DataInputStream(socket.getInputStream());

            // membuat data output stream
            OutputStream outputStream = socket.getOutputStream();
            dout = new DataOutputStream(outputStream);

            String chtServer = "";
            String chtClient = "";
            //pengkondisian perulangan chat
            while (!chtServer.equalsIgnoreCase("exit")) {
                //mengirim chat ke server
                chtServer = input.nextLine();
                dout.writeUTF(chtServer);
                dout.flush();
                //membaca chat server
                chtClient = din.readUTF();
                System.out.println("Pesan dari Server: " + chtClient);

            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            try {
                input.close();
                din.close();
                dout.close();
                socket.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
