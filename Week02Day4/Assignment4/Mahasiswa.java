package comp.assignment4;

import java.util.Scanner;

public class Mahasiswa {

    //mendefinisikan variabel
    String nama,jurusan;
    static int kimia;
    static int fisika;
    static int biologi;

    //membuat default constructor
    public Mahasiswa() {
        this.nama = "Default";
        this.jurusan = "Default";
        this.kimia = 0;
        this.fisika = 0;
        this.biologi = 0;
    }

    //membuat constructor dengan 2 parameter
    public Mahasiswa(String nama, String jurusan){
        this.nama=nama;
        this.jurusan=jurusan;
        this.kimia=0;
        this.fisika=0;
        this.biologi=0;
    }
    //membuat constructor dengan 5 parameter
    public Mahasiswa(String nama, String jurusan, int kimia, int fisika, int biologi){
        this.nama=nama;
        this.jurusan=jurusan;
        this.kimia=kimia;
        this.fisika=fisika;
        this.biologi=biologi;
    }
    public static void main(String args[]) {
        Scanner inputString = new Scanner(System.in);
        Scanner inputInt = new Scanner(System.in);

        //membuat perulangan untuk input menu
        int w = 1, loop = 2;
        while (w < loop) {
            System.out.println("MENU ");
            System.out.println("1. Konstruktor tanpa parameter ");
            System.out.println("2. Konstruktor dengan 2 parameter ");
            System.out.println("3. Konstruktor dengan 5 parameter ");
            System.out.println("4. EXIT ");
            System.out.println("");
            System.out.print("Input Menu : ");
            System.out.print("");

            int menu = inputInt.nextInt();
            switch (menu) {
                case 1:
                    // membuat object untuk memanggil constructor default dan menampilkan datanya
                    Mahasiswa data = new Mahasiswa();
                    System.out.println("Nama Mahasiswa = " + data.nama);
                    System.out.println("Nama Jurusan = " + data.jurusan);
                    System.out.println("Nilai Kimia = " + data.kimia);
                    System.out.println("Nilai Fisika = " + data.fisika);
                    System.out.println("Nilai Biologi = " + data.biologi);
                    break;
                case 2:
                    // membuat object untuk memanggil constructor 2 parameter dan menampilkan datanya
                    System.out.print("Input Nama = ");
                    String nama = inputString.nextLine();
                    System.out.print("Input Jurusan = ");
                    String jurusan = inputString.nextLine();
                    Mahasiswa data1 = new Mahasiswa(nama, jurusan);
                    System.out.println("Nama Mahasiswa = " + data1.nama);
                    System.out.println("Nama Jurusan = " + data1.jurusan);
                    System.out.println("Nilai Kimia = " + data1.kimia);
                    System.out.println("Nilai Fisika = " + data1.fisika);
                    System.out.println("Nilai Biologi = " + data1.biologi);
                    break;
                case 3:
                    //// membuat object untuk memanggil constructor 5 parameter dan menampilkan datanya
                    System.out.print("Input Nama = ");
                    nama = inputString.nextLine();
                    System.out.print("Input Jurusan = ");
                    jurusan = inputString.nextLine();
                    System.out.print("Input Nilai Kimia = ");
                    int kimia = inputInt.nextInt();
                    System.out.print("Input Nilai Fisika = ");
                    int fisika = inputInt.nextInt();
                    System.out.print("Input Nilai Biologi = ");
                    int biologi = inputInt.nextInt();
                    Mahasiswa data2 = new Mahasiswa(nama, jurusan, kimia, fisika, biologi);
                    System.out.println("Nama Mahasiswa = " + data2.nama);
                    System.out.println("Nama Jurusan = " + data2.jurusan);
                    System.out.println("Nilai Kimia = " + data2.kimia);
                    System.out.println("Nilai Fisika = " + data2.fisika);
                    System.out.println("Nilai Biologi = " + data2.biologi);
                    break;
                default:
                    System.out.println("== Anda telah Exit ==");
                    //mengubah variabel parameter untuk berhenti perulangan input menu
                    w = 10;
                    inputInt.close();
                    inputString.close();
                    break;


            }
        }

    }

}
