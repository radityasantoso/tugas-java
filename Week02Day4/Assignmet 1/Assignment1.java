import java.util.*; 
public class Assignment1{
    public static void main(String []args){
        //membuat class object dan variabelnya 
        class Mobil{
            String merk;
            String warna;
            Mobil(String merk,String warna){
                this.merk=merk;
                this.warna=warna;
            }
        //membuat method info mobil
        public void info(){
            System.out.println("Merk Mobil : "+merk);
            System.out.println("Warna Mobil : "+warna);
        }
        //membuat method berjalan
        public void jalan(){
            System.out.println("Mobil "+merk+" warna "+warna+" sedang berjalan");
            }
        //membuat method ber
         public void berhenti(){
            System.out.println("Mobil "+merk+" warna "+warna+" sedang berhenti");
            }
        }
        //membuat arraylist class mobil
        ArrayList<Mobil> data= new ArrayList<Mobil>();
        Scanner inputString = new Scanner(System.in);
        
        //input data mobil
        System.out.print("Input Merk Mobil : ");
        String merk= inputString.nextLine();
        System.out.print("Input Warna Mobil : ");
        String warna = inputString.nextLine();
        
        //membuat object data mobil
        Mobil dataMobil = new Mobil(merk, warna);

        //menambahkan data mobil
        data.add(dataMobil);

        //memanggil method di class mobil
        dataMobil.info();
        dataMobil.jalan();
        dataMobil.berhenti();
    
     }
}