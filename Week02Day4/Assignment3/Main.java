package comp.assignment3;

import java.util.Scanner;

public class Main {
    public static void main(String args[]) {
        //mendeklarasikan object hasil perhitungan berdasarkan method
        Volume result = new Volume();
        Scanner input = new Scanner(System.in);
        Scanner inputD = new Scanner(System.in);

        int num1 = 0;
        int num2 = 0;
        int num3 = 0;
        //perulangan untuk pilih menu
        int w = 1, loop = 2;
        while (w < loop) {
            System.out.println("MENU ");
            System.out.println("1. Volume Balok ");
            System.out.println("2. Volume Bola ");
            System.out.println("3. Volume Tabung ");
            System.out.println("4. EXIT ");
            System.out.println("");
            System.out.print("Input Menu : ");
            System.out.print("");
            //mendeklarasikan pilih menu
            int menu = input.nextInt();
            switch (menu) {
                case 1:
                    //input data perhitungan volume balok
                    System.out.print("Input Panjang = ");
                    num1 = input.nextInt();
                    System.out.print("Input Lebar = ");
                    num2 = input.nextInt();
                    System.out.print("Input Tinggi = ");
                    num3 = input.nextInt();
                    //cetak hasil perhitungan volume balok
                    System.out.println("Volume Balok = " + result.vol(num1, num2,num3));
                    break;
                case 2:
                    //input data perhitungan volume bola
                    System.out.print("Input Panjang = ");
                    num1 = input.nextInt();
                    System.out.print("Input Lebar = ");
                    num2 = input.nextInt();
                    //cetak hasil perhitungan volume bola
                    System.out.println("Volume Bola = " + result.vol(num1, num2));
                    break;
                case 3:
                    //input data perhitungan volume tabung
                    System.out.print("Input Panjang = ");
                    double dNum1 = inputD.nextDouble();
                    System.out.print("Input Lebar = ");
                    num2 = input.nextInt();
                    System.out.print("Input Tinggi = ");
                    num3= input.nextInt();
                    //cetak hasil perhitungan volume tabung
                    System.out.println("Volume Bola = " + result.vol(dNum1, num2, num3));
                    break;

                default:
                    System.out.println("== Anda telah Exit ==");
                    //mengubah variabel parameter untuk berhenti perulangan input menu
                    w = 10;
                    input.close();
                    inputD.close();
                    break;

            }
        }
    }
}
