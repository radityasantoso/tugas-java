package comp.assignment3;

public class Volume {
    //membuat method overloading pada class volume
    public int vol(int p, int l, int t){
        return p*l*t;
    }
    public double vol(double phi, double jari){
        return 4 * phi * jari * jari * jari / 3;
    }

    public double vol(double phi, int jari, int t){
        return (phi * jari * jari) * t;
    }
}
