package Assignment2;

import org.json.simple.JSONObject;
import properties.CrunchifyGetPropertyValues;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;

public class MyClient {
    public static void main(String[] args) throws IOException {
        Scanner inputI = new Scanner(System.in);
        Scanner inputS = new Scanner(System.in);
        ServerSocket ss = null;
        Socket s = null;
        int port = 0;
        String ip = "";
        CrunchifyGetPropertyValues config = new CrunchifyGetPropertyValues();
        System.out.println("Read Config");
        try{
            //connect to server
            port = Integer.parseInt(config.getPropValues("PORT"));
            ip = config.getPropValues("IP");
            s = new Socket(ip, port);

            DataOutputStream dout=new DataOutputStream(s.getOutputStream());

            int w = 1, loop = 2;
            while (w < loop) {
                System.out.println("MENU ");
                System.out.println("1. Registrasi ");
                System.out.println("2. Create Data");
                System.out.println("5. EXIT ");
                System.out.println("");
                System.out.print("Input Menu : ");
                System.out.print("");


                int menu = inputI.nextInt();
                switch (menu) {
                    case 1:
                        System.out.print("Nama : ");
                        String nama = inputS.next();
                        System.out.print("Email : ");
                        String email = inputS.next();
                        System.out.println("================");

                        boolean validUname = Pattern.matches("^([\\w-\\.]+){1,64}@([\\w&&[^_]]+){2,255}.[a-z]{2,}$", email);
                        System.out.println(validUname);
                        if (validUname) {
                            System.out.print("Password : ");
                            String pass = inputS.next();
                            boolean validPass = Pattern.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$", pass);
                            if (validPass) {
                                System.out.println("Anda Berhasil Login.....");
                                JSONObject jo = new JSONObject();
                                jo.put("mode", 1);
                                jo.put("username", email);
                                jo.put("password", pass);
                                jo.put("nama", nama);

                                dout=new DataOutputStream(s.getOutputStream());

                                String js = jo.toJSONString();

                                dout.writeUTF(js);
                                dout.flush();
                                dout.close();

                            } else {
                                System.out.println("Password Salah");
                            }
                        } else {
                            System.out.println("Username Salah");
                        }

                        break;
                    case 2:
                        System.out.print("Email : ");
                        String emaiLog = inputS.next();
                        System.out.println("================");
                        boolean validLog = Pattern.matches("^([\\w-\\.]+){1,64}@([\\w&&[^_]]+){2,255}.[a-z]{2,}$", emaiLog);
                        System.out.println(validLog);
                        if (validLog) {
                            System.out.print("Password : ");
                            String pass = inputS.next();
                            boolean validPass = Pattern.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$", pass);
                            if (validPass) {
                                System.out.println("Anda Berhasil Login.....");
                                JSONObject jo = new JSONObject();
                                jo.put("mode", 2);
                                jo.put("username", emaiLog);
                                jo.put("password", pass);
                                dout=new DataOutputStream(s.getOutputStream());

                                String js = jo.toJSONString();

                                dout.writeUTF(js);
                                dout.flush();
                                dout.close();

                            } else {
                                System.out.println("Password Salah");
                            }
                        } else {
                            System.out.println("Username Salah");
                        }

                        break;
                    case 3:

                        System.out.println("//-- Update Nilai --//");
                        System.out.print("Username : ");
                        email = inputS.next();
                        System.out.print("Password : ");
                        String pass = inputS.next();

                        System.out.print("Input nilai 1 :");
                        int n1 = inputI.nextInt();
                        System.out.print("Input nilai 2 :");
                        int n2 = inputI.nextInt();
                        System.out.print("Input nilai 3 :");
                        int n3 = inputI.nextInt();

                        ArrayList<Long> nilai = new ArrayList<>();
                        nilai.add((long) n1);
                        nilai.add((long) n2);
                        nilai.add((long) n3);


                        JSONObject dataUserLogin = new JSONObject();
                        dataUserLogin.put("mode", "3");
                        dataUserLogin.put("username", email);
                        dataUserLogin.put("password", pass);
                        dataUserLogin.put("nilai", nilai);

                        dout = new DataOutputStream(s.getOutputStream());
                        dout.writeUTF(String.valueOf(dataUserLogin));
                        dout.flush();

                        break;

                }
            }


            s.close();


        }catch(Exception e){System.out.println(e);}
    }
}
