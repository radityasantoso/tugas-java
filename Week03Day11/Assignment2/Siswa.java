package Assignment2;

import java.util.ArrayList;

public class Siswa {
    String username;
    String password;
    String name;

    ArrayList nilai = new ArrayList();

    public Siswa(String username, String password, String name, ArrayList nilai) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.nilai = nilai;
    }
    /*public Siswa( String b, String c, String d) {
        this.username = b;
        this.password = c;
        this.name = d;
    }*/

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return (
                "\nUsername : " + this.username +
                        "\nPassword : " + this.password +
                        "\nName : " + this.name +
                        "\nNilai : " + this.nilai);
    }
}
