import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

class JsonEncodeDemo {
    public static void main(String[] args){
        JSONArray arModel1 = new JSONArray();
        arModel1.add("Fiesta");
        arModel1.add("Focus");
        arModel1.add("Mustang");

        JSONArray arModel2 = new JSONArray();
        arModel2.add("320");
        arModel2.add("X3");
        arModel2.add("X5");

        JSONArray arModel3 = new JSONArray();
        arModel3.add("500");
        arModel3.add("Panda");

        JSONObject nmModel1 = new JSONObject();
        nmModel1.put("Name", "Ford");
        nmModel1.put("Models", arModel1);

        JSONObject nmModel2 = new JSONObject();
        nmModel2.put("Name", "BMW");
        nmModel2.put("Models", arModel2);

        JSONObject nmModel3 = new JSONObject();
        nmModel3.put("Name", "Fiat");
        nmModel3.put("Models", arModel3);

        JSONArray arNmCars = new JSONArray();
        arNmCars.add(nmModel1);
        arNmCars.add(nmModel2);
        arNmCars.add(nmModel3);

        JSONObject obj = new JSONObject();
        obj.put("name", "John");
        obj.put("agen", 30);
        obj.put("cars", arNmCars);

        System.out.print(obj);
    }
}
