import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;

public class JsonDecodeDemo2 {

    public static void main(String[] args) throws IOException, ParseException {

        /*
        {
            "name":"John",
            "age":30,
            "cars": ["Ford","BMW","Fiat"]
        }
        */

        JSONParser parser = new JSONParser();
        Reader reader = new StringReader("{\n" +
                " \"name\":\"John\",\n" +
                " \"age\":30,\n" +
                " \"cars\": [\n" +
                " { \"name\":\"Ford\", \"models\":[ \"Fiesta\", \"Focus\", \"Mustang\" ] },\n" +
                " { \"name\":\"BMW\", \"models\":[ \"320\", \"X3\", \"X5\" ] },\n" +
                " { \"name\":\"Fiat\", \"models\":[ \"500\", \"Panda\" ] }\n" +
                " ]\n" +
                " }\n");

        Object jsonObj = parser.parse(reader);

        JSONObject jsonObject = (JSONObject) jsonObj;


        String name = (String) jsonObject.get("name");
        System.out.println("Name = " + name);

        long age = (Long) jsonObject.get("age");
        System.out.println("Age = " + age);
        System.out.println("Cars = ");
        JSONArray jsonArray = (JSONArray) jsonObject.get("cars");
        for(int i=0; i<jsonArray.size();i++){
            JSONObject cars=(JSONObject) jsonArray.get(i);
            String nmMbl=(String) cars.get("name");
            System.out.println("Name = "+ nmMbl);
            JSONArray jsonArray1 = (JSONArray) cars.get("models");
            System.out.print("Model = ");
            for(Object el:jsonArray1){
                System.out.print(" "+el);

            }
            System.out.println("");

        }

        /*ArrayList<String> arCars = new ArrayList<>();
        arCars = (ArrayList<String>) jsonArray;
        //===================================


        for (String el:arCars) {
            JSONArray objModel = (JSONArray) jsonObject.get("models");
            JSONObject nmcar1 = (JSONObject) jsonObj;
            String car1 = (String) nmcar1.get("models");
            //String car1 = (String) objModel.get("model1"));
            System.out.println("name = Ford\n " +
                               "models = "+ car1);*/
            /*
            String namaCar = (String) nmcar1.get("name");
            System.out.println("Name = " + namaCar);*/


           /*
            ArrayList<String> arMod = new ArrayList<>();
            arMod = (ArrayList<String>) modelsArray;
            for(String il:arMod){
                JSONObject modelcar1 = (JSONObject) jsonObj;
                System.out.println("");

            }*/
            //System.out.println("Cars = " + el);

    }

}