package Assignment2;

import java.util.ArrayList;

abstract class Worker {
    int id;
    String nama;
    int tPulsa;
    int gapok;
    int absen;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int gettPulsa() {
        return tPulsa;
    }

    public void settPulsa(int tPulsa) {
        this.tPulsa = tPulsa;
    }

    public int getGapok() {
        return gapok;
    }

    public void setGapok(int gapok) {
        this.gapok = gapok;
    }

    public int getAbsen() {
        return absen;
    }

    public void setAbsen(int absen) {
        this.absen = absen;
    }
}

class Staff extends Worker{
    int tMakan;
    ArrayList <String> email = new ArrayList <> ();


    public int gettMakan() {
        return tMakan;
    }

    public void settMakan(int tMakan) {
        this.tMakan = tMakan;
    }

    public ArrayList<String> getEmail() {
        return email;
    }

    public void setEmail(ArrayList<String> email) {
        this.email = email;
    }
}

class Manager extends Worker{
    int tTransport;
    int tEntertaint;
    ArrayList <String> telepon = new ArrayList<>();

    public int gettTransport() {
        return tTransport;
    }

    public void settTransport(int tTransport) {
        this.tTransport = tTransport;
    }

    public int gettEntertaint() {
        return tEntertaint;
    }

    public void settEntertaint(int tEntertaint) {
        this.tEntertaint = tEntertaint;
    }

    public ArrayList<String> getTelepon() {
        return telepon;
    }

    public void setTelepon(ArrayList<String> telepon) {
        this.telepon = telepon;
    }
}

