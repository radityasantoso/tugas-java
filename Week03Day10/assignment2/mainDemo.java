package Assignment2;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class mainDemo {
    ArrayList<Manager> arrManager = new ArrayList<>();
    ArrayList<Staff> arrStaff = new ArrayList<>();

    Scanner inputI = new Scanner(System.in);
    Scanner inputS = new Scanner(System.in);

    void addDataStaff() {
        Staff newStaff = new Staff();
        ArrayList<String> email = new ArrayList<>();

        System.out.print("Input ID: ");
        int id = inputI.nextInt();
        System.out.print("Input Nama: ");
        String nama = inputS.next();
        System.out.print("Input Tunjangan Pulsa: ");
        int tPulsa = inputI.nextInt();
        System.out.print("Input Gaji Pokok: ");
        int gapok = inputI.nextInt();
        System.out.print("Input Absensi: ");
        int absen = inputI.nextInt();

        System.out.print("Input Tunjangan Makan: ");
        int tMakan = inputI.nextInt();

        System.out.print("Input Email Kantor: ");
        String emailKantor = inputS.next();
        System.out.print("Input Email Pribadi: ");
        String emailPribadi = inputS.next();
        email.add(emailKantor);
        email.add(emailPribadi);

        newStaff.setId(id);
        newStaff.setNama(nama);
        newStaff.settPulsa(tPulsa);
        newStaff.setGapok(gapok);
        newStaff.setAbsen(absen);
        newStaff.settMakan(tMakan);
        newStaff.setEmail(email);

        arrStaff.add(newStaff);
    }

    void addDataManager() {
        Manager newManager = new Manager();
        ArrayList<String> telepon = new ArrayList<>();

        System.out.print("Input ID: ");
        int id = inputI.nextInt();
        System.out.print("Input Nama: ");
        String nama = inputS.next();
        System.out.print("Input Tunjangan Pulsa: ");
        int tPulsa = inputI.nextInt();
        System.out.print("Input Gaji Pokok: ");
        int gapok = inputI.nextInt();
        System.out.print("Input Absensi: ");
        int absen = inputI.nextInt();
        System.out.print("Input Tunjangan Transport: ");
        int tTransport = inputI.nextInt();
        System.out.print("Input Tunjangan Entertaint: ");
        int tEntertaint = inputI.nextInt();
        System.out.print("Input Telepon HP: ");
        String tlpHp = inputS.next();
        System.out.print("Input Telepon Rumah: ");
        String tlpRumah = inputS.next();
        telepon.add(tlpHp);
        telepon.add(tlpRumah);

        newManager.setId(id);
        newManager.setNama(nama);
        newManager.settPulsa(tPulsa);
        newManager.setGapok(gapok);
        newManager.setAbsen(absen);
        newManager.settTransport(tTransport);
        newManager.settEntertaint(tEntertaint);
        newManager.setTelepon(telepon);

        arrManager.add(newManager);

    }

    void PrintStaff() throws IOException {
        System.out.println("Export Staff");

        JSONArray listStaff = new JSONArray();

        Iterator itr = arrStaff.iterator();
        while (itr.hasNext()) {
            JSONObject dataStaff = new JSONObject();
            Staff st = (Staff) itr.next();

            dataStaff.put("id", st.getId());
            dataStaff.put("nama", st.getNama());
            dataStaff.put("tunjPulsa", st.gettPulsa());
            dataStaff.put("gapok", st.getGapok());
            dataStaff.put("absensi", st.getAbsen());
            dataStaff.put("tunjMakan", st.gettMakan());
            dataStaff.put("email", st.getEmail());

            listStaff.add(dataStaff);
        }

        JSONObject objStaff = new JSONObject();
        objStaff.put("staff", listStaff);

        // Write to File
        FileOutputStream fout = new FileOutputStream("C:\\workspace\\staff.txt");
        byte b[] = String.valueOf(objStaff).getBytes();//converting string into byte array
        fout.write(b);
        fout.close();
        System.out.println("success...");

    }

    void PrintManager() throws IOException {
        System.out.println("Export Manager");
        JSONArray listManager = new JSONArray();

        Iterator itr = arrManager.iterator();
        while (itr.hasNext()) {
            JSONObject dataManager = new JSONObject();
            Manager mngr = (Manager) itr.next();

            dataManager.put("id", mngr.getId());
            dataManager.put("nama", mngr.getNama());
            dataManager.put("tunjPulsa", mngr.gettPulsa());
            dataManager.put("gapok", mngr.getGapok());
            dataManager.put("absensi", mngr.getAbsen());
            dataManager.put("tunjTransport", mngr.gettTransport());
            dataManager.put("tunjEntertaint", mngr.gettEntertaint());
            dataManager.put("telepon", mngr.getTelepon());

            listManager.add(dataManager);
        }

        JSONObject objManager = new JSONObject();
        objManager.put("manager", listManager);

        // Write to File
        FileOutputStream fout = new FileOutputStream("C:\\workspace\\manager.txt");
        byte b[] = String.valueOf(objManager).getBytes();//converting string into byte array
        fout.write(b);
        fout.close();
        System.out.println("success...");
    }


    public static void main(String[] args) throws IOException {
        Scanner input = new Scanner(System.in);
        mainDemo obj = new mainDemo();

        int userInput = 0;

        while (userInput != 99) {
            System.out.println("Menu");
            System.out.println("1. Buat Worker");
            System.out.println("2. Create JSON Format and Write to File");
            System.out.println("3. Read JSON Format from a File, input Filename(Show on Screen)");
            System.out.println("99. EXIT");
            System.out.print("Pilih Menu: ");
            userInput = input.nextInt();

            switch (userInput) {
                case 1:
                    System.out.println("a. Buat Staff");
                    System.out.println("b. Buat Manager");
                    System.out.print("Pilih: ");
                    String pilih = input.next();

                    if (pilih.equalsIgnoreCase("a")) {
                        obj.addDataStaff();
                    }else if (pilih.equalsIgnoreCase("b")) {
                        obj.addDataManager();
                    }
                    break;
                case 2:
                    System.out.println("a. Print Staff");
                    System.out.println("b. Print Manager");
                    System.out.print("Pilih: ");
                    String pilih2 = input.next();

                    if (pilih2.equalsIgnoreCase("a")) {
                        obj.PrintStaff();
                    }else if (pilih2.equalsIgnoreCase("b")) {
                        obj.PrintManager();
                    }
                    break;

            }
        }
    }
}
