package comp.tugasweekend;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

class Mahasiswa{
    int id;
    String nama;
    //double  inggris, fisika, algoritma;
    ArrayList nilai = new ArrayList();
    public Mahasiswa(int id, String nama, ArrayList nilai) {
        this.id = id;
        this.nama = nama;
        this.nilai = nilai;
    }
   /* double inggris, fisika, algoritma;

    public double getInggris() {
        return inggris;
    }

    public void setInggris(double inggris) {
        this.inggris = inggris;
    }

    public double getFisika() {
        return fisika;
    }

    public void setFisika(double fisika) {
        this.fisika = fisika;
    }

    public double getAlgoritma() {
        return algoritma;
    }

    public void setAlgoritma(double algoritma) {
        this.algoritma = algoritma;
    }

    public Mahasiswa(double inggris, double fisika, double algoritma) {
        this.inggris=inggris;
        this.fisika=fisika;
        this.algoritma=algoritma;
    }*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public ArrayList getNilai() {
        return nilai;
    }

    public void setNilai(ArrayList nilai) {
        this.nilai = nilai;
    }
}

class VwData extends Thread{
    private Map map;

    public VwData (Map map){
        this.map=map;
    }
    public void run(){
        TreeMap<Integer, Mahasiswa> sorted = new TreeMap<>(this.map);
        //Converting to Set so that we can traverse
        Iterator itr=sorted.keySet().iterator();
        System.out.println("Id\t Nama\tBhs.Inggris\tFisika\tAlgoritma");

        while(itr.hasNext()){
            int key = (int) itr.next();
            Mahasiswa mhs = (Mahasiswa) this.map.get(key);
            System.out.println(mhs.getId() + "\t" + mhs.getNama() + "\t\t" + mhs.getNilai().get(0) + "\t\t" + mhs.getNilai().get(1) + "\t\t" + mhs.getNilai().get(2));
        }
    }
}

class PrintData extends  Thread{
    private Map map;

    public PrintData (Map map){
        this.map=map;
    }
    public void run(){
        Assignment3 obj2 = new Assignment3();
        try {
            obj2.addLoct();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}


public class Assignment3  {

    Map map = new HashMap();
    Scanner inputI = new Scanner(System.in);
    Scanner inputS = new Scanner(System.in);

    void addData(){
        ArrayList nilai = new ArrayList();
        System.out.print("Input ID : ");
        int id = this.inputI.nextInt();
        System.out.print("Input Nama : ");
        String nama = this.inputS.nextLine();
        System.out.print("Input Nilai Bhs.Inggris : ");
        double inggris = inputI.nextDouble();
        nilai.add(0,inggris);
        System.out.print("Input Nilai Fisika : ");
        double fisika = inputI.nextDouble();
        nilai.add(1,fisika);
        System.out.print("Input Nilai Algoritma : ");
        double algoritma = inputI.nextDouble();
        nilai.add(2,algoritma);
        //nilai.add(new Mahasiswa(inggris, fisika, algoritma));
        map.put(id,new Mahasiswa(id,nama,nilai));

    }
    void viewData(){
        Set set=map.entrySet();//Converting to Set so that we can traverse
        Iterator itr=set.iterator();
        System.out.println("Id\t Nama\tBhs.Inggirs\tFisika\tAlgoritma");

        while(itr.hasNext()){

            //Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry=(Map.Entry)itr.next();
            //Convert to Staff
            Mahasiswa st = (Mahasiswa) entry.getValue();
            TreeMap<Integer, Mahasiswa> sorted = new TreeMap<>();
            sorted.putAll(map);
            //ArrayList arNilai = st.getNilai();
            //Print data
            System.out.println(entry.getKey()+"\t"+st.getNama()+"\t\t"+st.nilai.get(0)+"\t\t"+st.nilai.get(1)+"\t\t"+st.nilai.get(2));
        }
    }
    void addLoct () throws IOException {
        try {
            System.out.print("Masukan lokasi penyimpanan : ");
            String disk = inputS.next();
            System.out.print("Masukan Nama File : ");
            String nmFile = inputS.next();
            FileWriter writer = new FileWriter("C:\\" + disk + "\\" + nmFile + ".txt");
            BufferedWriter buffer = new BufferedWriter(writer);
            Set set = map.entrySet();//Converting to Set so that we can traverse
            Iterator itr = set.iterator();
            buffer.write("Id\t Nama\tBhs.Inggirs\tFisika\tAlgoritma\n");

            while (itr.hasNext()) {

                //Converting to Map.Entry so that we can get key and value separately
                Map.Entry entry = (Map.Entry) itr.next();
                //Convert to Staff
                Mahasiswa st = (Mahasiswa) entry.getValue();
                TreeMap<Integer, Mahasiswa> sorted = new TreeMap<>();
                sorted.putAll(map);
                //Print data
                buffer.write(entry.getKey() + "\t" + st.getNama() + "\t\t" + st.nilai.get(0) + "\t\t"
                        + st.nilai.get(1) + "\t\t" + st.nilai.get(2)+"\n");
            }
            buffer.close();
            writer.close();
        }catch(Exception e){System.out.println(e);}

        System.out.println("Success");
    }


    public static void main(String args[]) throws IOException {
        Map map = new HashMap();

        Scanner input = new Scanner(System.in);
        Assignment3 obj = new Assignment3();

        //membuat perulangan untuk input menu
        int w = 1, loop = 2;
        while (w < loop) {
            System.out.println("MENU ");
            System.out.println("1. Input Data Mahasiswa");
            System.out.println("2. View Laporan Nilai Data Mahasiswa ");
            System.out.println("3. View Laporan and Create File ");
            System.out.println("4. EXIT ");
            System.out.println("");
            System.out.print("Input Menu : ");
            System.out.print("");

            int menu = input.nextInt();
            switch (menu) {
                case 1:
                    obj.addData();
                    break;
                case 2:
                    obj.viewData();
                    break;
                case 3:
                    VwData t1 = new VwData(map);
                    t1.start();
                    PrintData tt = new PrintData(map);
                    tt.start();

                    break;
                default :
                    System.out.println("== Anda telah Exit ==");
                    w= 10;
                    inputInt.close();
                    inputString.close();
                    
                break;

            }
        }

    }

}
