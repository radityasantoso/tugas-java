package comp.week2day8;
//membuat thread pertama
class G2Academy extends Thread{
    //create method run
    public void run(){
        for(int i=1;i<=10;i++){
            //running setiap setengah detik
            try{Thread.sleep(500);}catch(InterruptedException e){System.out.println(e);}
            System.out.println("G2 Academy");
        }
    }
}
//membuat thread ke dua
class PrntAngka extends Thread{
    /// create method run
    public void run(){
        for(int i=1;i<=10;i++){
            //running setiap setengah detik
            try{Thread.sleep(500);}catch(InterruptedException e){System.out.println(e);}
            System.out.println(i);
        }
    }
}


public class AssignmentThread{

    public static void main(String args[]){
        //create object thread
        G2Academy t1=new G2Academy();
        PrntAngka t2= new PrntAngka();
        //call method run
        t1.start();
        t2.start();
    }
}

