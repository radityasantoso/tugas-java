package comp.tugasweekend;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.*;

class Mhs {
    int id;
    String nama;
    //double  inggris, fisika, algoritma;
    ArrayList nilai = new ArrayList();
    public Mhs(int id, String nama, ArrayList nilai) {
        this.id = id;
        this.nama = nama;
        this.nilai = nilai;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public ArrayList getNilai() {
        return nilai;
    }

    public void setNilai(ArrayList nilai) {
        this.nilai = nilai;
    }
}

public class Assignment4 {
    Map map = new HashMap();
    Scanner inputI = new Scanner(System.in);
    Scanner inputS = new Scanner(System.in);
    void addData(){
        ArrayList nilai = new ArrayList();
        System.out.print("Input ID : ");
        int id = this.inputI.nextInt();
        System.out.print("Input Nama : ");
        String nama = this.inputS.nextLine();
        System.out.print("Input Nilai Bhs.Inggris : ");
        double inggris = inputI.nextDouble();
        nilai.add(0,inggris);
        System.out.print("Input Nilai Fisika : ");
        double fisika = inputI.nextDouble();
        nilai.add(1,fisika);
        System.out.print("Input Nilai Algoritma : ");
        double algoritma = inputI.nextDouble();
        nilai.add(2,algoritma);
        //nilai.add(new Mahasiswa(inggris, fisika, algoritma));
        map.put(id,new Mhs(id,nama,nilai));

    }
    void viewData(){
        Set set=map.entrySet();//Converting to Set so that we can traverse
        Iterator itr=set.iterator();
        System.out.println("Id\t Nama\tBhs.Inggirs\tFisika\tAlgoritma");

        while(itr.hasNext()){

            //Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry=(Map.Entry)itr.next();
            //Convert to Staff
            Mhs st = (Mhs) entry.getValue();
            TreeMap<Integer, Mhs> sorted = new TreeMap<>();
            sorted.putAll(map);
            //ArrayList arNilai = st.getNilai();
            //Print data
            System.out.println(entry.getKey()+"\t"+st.getNama()+"\t\t"+st.nilai.get(0)+"\t\t"+st.nilai.get(1)+"\t\t"+st.nilai.get(2));
        }
    }
    void editData(){

        System.out.print("Masukan ID yang akan di Edit : ");
        int id = this.inputI.nextInt();
        Set set=map.entrySet();//Converting to Set so that we can traverse
        Iterator itr=set.iterator();
        while(itr.hasNext()){
            Map.Entry entry=(Map.Entry)itr.next();
            Mhs st = (Mhs) entry.getValue();
            if(st.getId()==id){
                System.out.println("Id Mahasiswa : "+st.getId());
                System.out.print("Edit Nama Mahasiswa : ");
                String nama = this.inputS.nextLine();
                st.setNama(nama);
                System.out.println("Data berhasil diubah menjadi "+nama);
            }
        }
    }
    void deleteData(){

        System.out.print("Masukan ID yang akan di Edit : ");
        int id = this.inputI.nextInt();
        Set set=map.entrySet();//Converting to Set so that we can traverse
        Iterator itr=set.iterator();
        while(itr.hasNext()){
            Map.Entry entry=(Map.Entry)itr.next();
            Mhs st = (Mhs) entry.getValue();
            if(st.getId()==id){
                System.out.println("Id yang di hapus : "+st.getId());
                map.remove(id);
                System.out.println("Data "+id+" Berhasil Di Hapus");
            }
        }
    }






    public static void main(String args[]) throws IOException {
        Scanner inputI = new Scanner(System.in);
        Scanner inputS = new Scanner(System.in);
        System.out.print("Masukan lokasi penyimpanan : ");
        String disk = inputS.next();
        System.out.print("Masukan Nama File : ");
        String nmFile = inputS.next();
        FileReader fr;
        BufferedReader br;
        String uname = "";
        String pass = "";

        try {
            fr = new FileReader("C:\\" + disk + "\\" + nmFile + ".txt");
            br = new BufferedReader(fr);
            int i;
            while ((i = br.read()) != -1) {
                uname = uname + (char) i;
            }
            System.out.println(uname);
            System.out.println("Success......");
            br.close();
            fr.close();
        } catch (IOException e) {
            System.out.println(e);
        }
        System.out.print("Masukan lokasi penyimpanan : ");
        String disk2 = inputS.next();
        System.out.print("Masukan Nama File : ");
        String nmFile2 = inputS.next();
        try {
            fr = new FileReader("C:\\" + disk2 + "\\" + nmFile2 + ".txt");
            br = new BufferedReader(fr);
            int i;
            while ((i = br.read()) != -1) {
                pass = pass + (char) i;
            }
            System.out.println(pass);
            System.out.println("Success......");
            br.close();
            fr.close();
        } catch (IOException e) {
            System.out.println(e);
        }
        //boolean test1=Pattern.compile("[\\w@\\w]*\\.com").matcher(uname).matches();
        int w = 1, loop = 2;
        while(w<loop){
            System.out.print("Username : ");
            String unameSU = inputS.next();
            boolean validUname = Pattern.matches("[\\w@\\w]*\\.com",unameSU);
            if(uname.equals(unameSU) && validUname==true){
                System.out.print("Password : ");
                String passSU= inputS.next();
                boolean validPass = Pattern.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$",passSU);
                if(pass.equals(passSU) && validPass==true){
                    System.out.println("Anda Berhasil Login.....");
                    w =3;
                }else{
                    System.out.println("Password Salah");
                    w=1;
                }
            }else {
                System.out.println("Username Salah");
                w=1;
            }

        }

        Assignment4 obj = new Assignment4();

        //membuat perulangan untuk input menu
        int a = 1, lup = 2;
        while (a < lup) {
            System.out.println("MENU ");
            System.out.println("1. Input Data Mahasiswa");
            System.out.println("2. Edit DAta MAhasiswa ");
            System.out.println("3. View Laporan Nilai Data Mahasiswa ");
            System.out.println("4. EXIT ");
            System.out.println("");
            System.out.print("Input Menu : ");
            System.out.print("");

            int menu = inputI.nextInt();
            switch (menu) {
                case 1:
                    obj.addData();
                    break;
                case 2:
                    obj.editData();
                    break;
                case 3:
                    obj.viewData();
                    break;
                default :
                    System.out.println("== Anda telah Exit ==");
                    a= 10;
                    inputI.close();
                    inputS.close();
                    break;
            }
        }

    }
}
