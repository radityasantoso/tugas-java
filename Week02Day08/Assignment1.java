package comp.week2day8;

import java.util.Scanner;
import java.util.regex.Pattern;

public class Assignment2 {
    public static void main(String args[]) {
        //input email
        Scanner input = new Scanner(System.in);
        System.out.print("Input Email: ");
        String email = input.next();
        //melakukan validasi
        boolean test1 = Pattern.matches("[\\w@\\w]*\\.com",email);
        if(test1){
            //validasi email benar
            System.out.println("Email benar");
            //input password
            System.out.print("Password: ");
            String pass = input.next();
            // melakukan validasi
            boolean test2 =Pattern.matches("([a-zA-Z\\W])*.{8,30}$",pass);

            if(test2){
                //validasi password benar
                System.out.println("Berhasil Login");
            } else{
                //validasi password salah
                System.out.println("Regex Salah");
            }
        } else{
            //validasi email salah
            System.out.println("Regex Salah");
        }

    }
}
