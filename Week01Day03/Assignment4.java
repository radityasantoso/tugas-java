import java.util.*; 
public class TugasRumah {
    public static void main(String args[]){
        
        //membuat class object dan variabelnya
        class Mobil{
            String merk;
            String warna;
            int tahun;
            Mobil(String merk,String warna, int tahun){
                this.merk=merk;
                this.warna=warna;
                this.tahun=tahun;
            }
        }
        Scanner inputString = new Scanner(System.in);
        Scanner inputInt = new Scanner(System.in);
        
        //mendeklarasikan array list
        ArrayList<Mobil> data= new ArrayList<Mobil>();
        
        int w=1, loop=2;
        int i = 1;
        int j=2;
        while(w<loop){
            System.out.println("MENU ");
            System.out.println("1. Input Object ");
            System.out.println("2. Tampilkan Object ");
            System.out.println("3. EXIT ");
            System.out.println("");
            System.out.print("Input Menu : ");
            System.out.print("");
            
            //mendeklarasikan switch menu
            int menu = inputInt.nextInt();
            switch (menu) {
                
                //membuat menu input data object mobil
                case 1:
                    System.out.print("Input Merk Mobil : ");
                    String merk= inputString.nextLine();

                    System.out.print("Input Warna Mobil : ");
                    String warna = inputString.nextLine();

                    System.out.print("Input Tahun Pembuatan Mobil : ");
                    int tahun = inputInt.nextInt();
                    
                    Mobil dataMobil = new Mobil(merk, warna, tahun);
                    data.add(dataMobil);
                    break;
                    
                    //membuat menu menampilkan data object mobil
                case 2:
                   Iterator itr = data.iterator();
                    while(itr.hasNext()){
                        Mobil viewMobil=(Mobil)itr.next();
                        // memberikan nomor urut
                        if (i<j){
                          System.out.println("");
                          System.out.println(" Data Nomor "+ i);
                          System.out.println("=================");
                           ++i;
                           ++j;
                        }
                        //cetak data object mobil
                        System.out.println("Merk : "+ viewMobil.merk);
                        System.out.println("Warna : "+ viewMobil.warna);
                        System.out.println("Tahun Pembuatan : "+ viewMobil.tahun);
                        System.out.println("");
                        
                    }
                break;
                //menu exit
                default :
                    System.out.println("== Anda telah Exit ==");
                    w= 10;
                    inputInt.close();
                    inputString.close();
                    
                break;
            }
         
        }
        
    }
}
