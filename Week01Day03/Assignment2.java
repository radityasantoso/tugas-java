import java.util.Scanner;
public class Array2Dimensi{

     public static void main(String []args){
        Scanner input = new Scanner(System.in);
        
        //input baris array
        System.out.print("Input Baris Array : ");
        int baris = input.nextInt();

        //input kolom array
        System.out.print("Input Kolom Array : ");
        int kolom = input.nextInt();
        
        //mendeklarasikan array 2 dimensi
        int [][] iArray = new int [baris][kolom];
        
        //perulangan untuk menginput nilai array berdasarkan index
        for(int i=0; i<iArray.length; i++){
        
                for(int j=0; j<iArray[i].length; j++){
                    System.out.print("Index ["+i+"]["+j+"] :");
                    iArray[i][j] = input.nextInt();
                }
        }
        
        System.out.println("Baris dan Kolom Array 2 dimensi adalah ["+baris+"]["+kolom+"]");
        
        //perulangan untuk cetak isi array 2 dimensi
        for(int [] arrayDimensi: iArray) {
            for(int angka:arrayDimensi) {
                System.out.print(angka + "\t");
      }
      System.out.println(" ");
    }
     }
}
