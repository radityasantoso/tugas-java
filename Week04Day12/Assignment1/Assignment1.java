package Assignment1;

import java.sql.*;
import java.io.*;
import java.util.Scanner;

class Assignment1{

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            // 1. Register Driver Class
            Class.forName("com.mysql.cj.jdbc.Driver");
            // 2. Creating Connection
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sonoo", "root", "bhinatungka");
            // 2.5 Set Auto Commit
            con.setAutoCommit(false);

            //here sonoo is database name, root is username and password
            // 3. Create Statement


            Statement stmt = con.createStatement();

            int userInput = 0;
            String nama = "";
            int umur = 0;
            int id = 0;
            String sql = "";
            String umurString = "";

            while (userInput != 99) {
                System.out.println("MENU");
                System.out.println("1. Show All Record");
                System.out.println("2. Insert Record");
                System.out.println("3. Update Record");
                System.out.println("4. Delete Record");
                System.out.println("99. Exit");

                System.out.print("Pilih Menu: ");
                userInput = input.nextInt();

                switch (userInput) {
                    case 1:
                        System.out.println("Show All Record");
                        ResultSet rs = stmt.executeQuery("select * from emp");
                        while (rs.next())
                            System.out.println(rs.getInt(1) + "  " + rs.getString(2) + "  " + rs.getString(3));
                        break;

                    case 2:
                        try {
                            System.out.println("Insert Record");
                            System.out.print("Masukan Nama: ");
                            nama = input.next();
                            System.out.print("Masukan Umur: ");
                            umurString = input.next();

                            sql = "insert into emp(name,age) values(?,?)";
                            PreparedStatement ps = con.prepareStatement(sql);

                            ps.setString(1, nama);
                            ps.setInt(2, Integer.parseInt(umurString));

                            ps.executeUpdate();

                        } catch (Exception e) {
                            System.out.println("RollBack");
                            con.rollback();
                        } finally {
                            if (Integer.parseInt(umurString) > 50) {    //kondisi seleksi umur
                                System.out.println("Umur lebih dari 50, rollback");
                                con.rollback(); // rollback umur lebih dari 50
                            }
                            con.commit();
                        }

                        break;

                    case 3:
                        System.out.println("Update Record");
                        System.out.print("Masukan ID yang mau diupdate: ");
                        id = input.nextInt();

                        System.out.println("A. Update Nama");
                        System.out.println("B. Update Umur");
                        System.out.println("C. Update Nama & Umur");
                        System.out.print("Pilih: ");
                        String pilih = input.next();

                        if (pilih.equalsIgnoreCase("a")) {
                            System.out.print("Update Nama: ");
                            nama = input.next();

                            sql = "update emp set name = ? where id = ?";
                            PreparedStatement psUpdate = con.prepareStatement(sql);

                            psUpdate.setString(1, nama);
                            psUpdate.setInt(2, id);

                            psUpdate.executeUpdate();
                            con.commit();


                        } else if (pilih.equalsIgnoreCase("b")) {
                            try {
                                System.out.println("Update Record");
                                System.out.print("Masukan Umur: ");
                                umurString = input.next();

                                sql = "update emp set age = ? where id = ?";
                                PreparedStatement psUpdate = con.prepareStatement(sql);

                                psUpdate.setInt(1, Integer.parseInt(umurString));
                                psUpdate.setInt(2, id);

                                psUpdate.executeUpdate();

                            } catch (Exception e) {
                                System.out.println("RollBack");
                                con.rollback();
                            } finally {
                                if (Integer.parseInt(umurString) > 50) {//kondisi seleksi umur
                                    System.out.println("Umur lebih dari 50, rollback");
                                    con.rollback();// rollback umur lebih dari 50
                                }
                                con.commit();
                            }

                        } else if (pilih.equalsIgnoreCase("c")) {
                            try {
                                System.out.println("Update Record");
                                System.out.print("Update Nama: ");
                                nama = input.next();
                                System.out.print("Update Umur: ");
                                umurString = input.next();

                                sql = "update emp set name = ?, age=? where id = ?";
                                PreparedStatement psUpdate = con.prepareStatement(sql);

                                psUpdate.setString(1, nama);
                                psUpdate.setInt(2, Integer.parseInt(umurString));
                                psUpdate.setInt(3, id);

                                psUpdate.executeUpdate();

                            } catch (Exception e) {
                                System.out.println("RollBack");
                                con.rollback();
                            } finally {
                                if (Integer.parseInt(umurString) > 50) {//kondisi seleksi umur
                                    System.out.println("Umur lebih dari 50, rollback");
                                    con.rollback();// rollback umur lebih dari 50
                                }
                                con.commit();
                            }
                        }

                        break;

                    case 4:
                        System.out.println("Delete Record");
                        System.out.print("Masukan ID yang mau didelete: ");
                        id = input.nextInt();

                        sql = "DELETE FROM emp WHERE id=?";
                        PreparedStatement psDelete = con.prepareStatement(sql);
                        psDelete = con.prepareStatement(sql);

                        psDelete.setInt(1, id);

                        psDelete.executeUpdate();
                        con.commit();
//                        stmt.executeUpdate("DELETE FROM emp WHERE id="+id+"");
                        break;
                }
            }

        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
