package Assignment2.model;

public class Mahasiswa {
    int id;

    public Mahasiswa(int id, String nama, double biologi, double fisika, double kimia) {
        this.id = id;
        this.nama = nama;
        this.biologi = biologi;
        this.fisika = fisika;
        this.kimia = kimia;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    String nama;
    double biologi;
    double fisika;

    double ratarata;

    public double getRatarata() {
        return ratarata;
    }

    public void setRatarata(double ratarata) {
        this.ratarata = ratarata;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public double getBiologi() {
        return biologi;
    }

    public void setBiologi(double biologi) {
        this.biologi = biologi;
    }

    public double getFisika() {
        return fisika;
    }

    public void setFisika(double fisika) {
        this.fisika = fisika;
    }

    public double getKimia() {
        return kimia;
    }

    public void setKimia(double kimia) {
        this.kimia = kimia;
    }

    double kimia;
}
