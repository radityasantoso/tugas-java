package Assignment2.view;

import Assignment2.controller.DaftarMahasiswa;
import Assignment2.controller.NotifikasiMahasiswa;

import java.util.Scanner;

public class MahasiswaApp {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        DaftarMahasiswa mc = new DaftarMahasiswa();
        NotifikasiMahasiswa nm = new NotifikasiMahasiswa();
        int pil=0;
        do {
            System.out.println("MENU");
            System.out.println("1. Input Data Mahasiswa");
            System.out.println("2. Hitung Rata Rata Nilai");
            System.out.println("3. Create File Data Staff");

            System.out.println("99. EXIT");

            System.out.print("Input nomor : ");
            pil = input.nextInt();
            switch (pil) {
                // performs addition between numbers
                case 1:
                    mc.addMhs();
                    break;
                case 2:
                    mc.NilaiRataRata();
                    break;
                case 3:
                    mc.Kelulusan();
                    break;
                case 4:
                    nm.PrintNotif();
                    break;

            }
            System.out.println();

        } while(pil != 99);

        // closing the scanner object
        input.close();
    }
}
