package Assignment2.controller;

import Assignment2.model.Mahasiswa;

import java.util.ArrayList;
import java.util.Scanner;

public class DaftarMahasiswa {
    ArrayList<Mahasiswa> arrMhs = new ArrayList<>();
    Scanner input = new Scanner(System.in);

    public void addMhs(){
        System.out.print("Input ID : ");
        int id = this.input.nextInt();
        System.out.print("Input Nama Mahasiswa : ");
        String nama = this.input.next();
        System.out.print("Input Nilai Biologi: ");
        double biologi = this.input.nextInt();
        System.out.print("Input Nilai Fisika: ");
        double fisika = this.input.nextInt();
        System.out.print("Input Nilai Kimia: ");
        double kimia = this.input.nextInt();
        //membuat object untuk memasukan data ke arraylist
        Mahasiswa st = new Mahasiswa(id, nama, biologi, fisika, kimia);
        this.arrMhs.add(st);

    }

    public void NilaiRataRata(){
        int i=0;
        for(Mahasiswa mhs:this.arrMhs){

            double ratarata =mhs.getBiologi() + mhs.getFisika() + mhs.getKimia() / 3;
            this.arrMhs.get(i).setRatarata(ratarata);
            i++;
        }
        System.out.println("Nilai Rata - Rata berhasil di Hitung....");
    }

    public void Kelulusan(){
        System.out.println("Cek kelulusan Mahasisa");
        System.out.println("==========================");
        System.out.print("Masukan ID Mahasiswa: ");
        int id = this.input.nextInt();

        for (Mahasiswa st:this.arrMhs) {

            if(id == st.getId()){

                if(st.getRatarata()<7){
                System.out.print("Maaf "+st.getNama()+" tidak lolos");

                }else{
                    System.out.print("Selamat "+st.getNama()+" lolos");
                }

                break;
            }

        }
    }



}
