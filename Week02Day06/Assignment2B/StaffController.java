package comp.week2day6.fix2.controller;

import comp.week2day6.fix2.model.Staff;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class StaffController {

    ArrayList<Staff> arrStaff = new ArrayList<>();

    Scanner input = new Scanner(System.in);

    public void createDummy(){
        System.out.println("Creating Dummy Data");
        this.arrStaff.add(new Staff(2,"PETER",3000));
        this.arrStaff.add(new Staff(1,"JOHN",2000));
    }

    //membuat method reader file
    public void readerFile(){
        //input lokasi penyimpanan dan nama file
        System.out.print("Masukan lokasi penyimpanan : ");
        String disk = input.next();
        System.out.print("Masukan Nama File : ");
        String nmFile = input.next();
        try {
            FileReader fr=new FileReader("C:\\"+disk + "\\"+nmFile+".txt");
            BufferedReader br=new BufferedReader(fr);
            //perulangan untuk membaca file ke console
            int i;
            String str = "";
            while((i=br.read())!=-1){
                str = str + (char)i;
            }
            //split string dengan parameter \n
            String [] parsedS = str.split("\n");
            //perulangan untuk menentukan  split string dengan parameter ","
            for (int index = 1; index < parsedS.length; index++ ){
                String [] parsedS2 = parsedS[index].split(",");
                //convert data strin  index 0 dan 2 menjadi integer
                int id = Integer.parseInt(parsedS2[0]);
                int totalGaji = Integer.parseInt(parsedS2[2]);
                //membuat object untuk dimasukan ke array list
                Staff st = new Staff(id, parsedS2[1], totalGaji);
                arrStaff.add(st);
                System.out.println("======================");
            }
            br.close();
            fr.close();
            System.out.println("Success");
        //menampilkan notifikasi error
        }catch (Exception e){
            System.out.println(e);
        }

    }
    //Membuat method untuk menampilkan data staff
    public void viewStaff(){
        for (Staff data: arrStaff) {
            System.out.println("Id : "+ data.getId());
            System.out.println("Nama Staff : "+ data.getId());
            System.out.println("Total Gaji : "+ data.getTotalGaji());
            String terbilang = angkaToTerbilang((long)data.getTotalGaji());
            System.out.println("Total Gaji : "+ terbilang);
        }

    }
    //membuat array variabel string untuk menampuk data terbilang
    static String[] huruf={"","Satu","Dua","Tiga","Empat","Lima","Enam","Tujuh","Delapan","Sembilan","Sepuluh","Sebelas"};
    //method untuk mentukan terbilang dengan melakukan perbandingan menggunakan if
    public String angkaToTerbilang(Long angka){
        if(angka < 12)
            return huruf[angka.intValue()];
        if(angka >=12 && angka <= 19)
            return huruf[angka.intValue() % 10] + " Belas";
        if(angka >= 20 && angka <= 99)
            return angkaToTerbilang(angka / 10) + " Puluh " + huruf[angka.intValue() % 10];
        if(angka >= 100 && angka <= 199)
            return "Seratus " + angkaToTerbilang(angka % 100);
        if(angka >= 200 && angka <= 999)
            return angkaToTerbilang(angka / 100) + " Ratus " + angkaToTerbilang(angka % 100);
        if(angka >= 1000 && angka <= 1999)
            return "Seribu " + angkaToTerbilang(angka % 1000);
        if(angka >= 2000 && angka <= 999999)
            return angkaToTerbilang(angka / 1000) + " Ribu " + angkaToTerbilang(angka % 1000);
        if(angka >= 1000000 && angka <= 999999999)
            return angkaToTerbilang(angka / 1000000) + " Juta " + angkaToTerbilang(angka % 1000000);
        if(angka >= 1000000000 && angka <= 999999999999L)
            return angkaToTerbilang(angka / 1000000000) + " Milyar " + angkaToTerbilang(angka % 1000000000);
        if(angka >= 1000000000000L && angka <= 999999999999999L)
            return angkaToTerbilang(angka / 1000000000000L) + " Triliun " + angkaToTerbilang(angka % 1000000000000L);
        if(angka >= 1000000000000000L && angka <= 999999999999999999L)
            return angkaToTerbilang(angka / 1000000000000000L) + " Quadrilyun " + angkaToTerbilang(angka % 1000000000000000L);
        return "";
    }

}
