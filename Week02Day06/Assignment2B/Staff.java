package comp.week2day6.fix2.model;

public class Staff {

    //Essential
    int id;
    String nama;
    int gapok;

    int absensi = 20;
    //membuat constructor Staff
    public Staff(int id, String nama, int totalGaji) {
        this.id = id;
        this.nama = nama;
        this.totalGaji = totalGaji;
    }

    //Calculated
    int tunjMakan;
    int tunjTransport;
    int totalGaji;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getGapok() {
        return gapok;
    }

    public void setGapok(int gapok) {
        this.gapok = gapok;
    }

    public int getAbsensi() {
        return absensi;
    }

    public void setAbsensi(int absensi) {
        this.absensi = absensi;
    }

    public int getTunjMakan() {
        this.tunjMakan = absensi *20000;
        return tunjMakan;
    }

    public void setTunjMakan(int tunjMakan) {
        this.tunjMakan = tunjMakan;
    }

    public int getTunjTransport() {
        this.tunjTransport=absensi+50000;
        return tunjTransport;
    }

    public void setTunjTransport(int tunjTransport) {
        this.tunjTransport = tunjTransport;
    }

    public int getTotalGaji() {
        return totalGaji;
    }

    public void setTotalGaji(int totalGaji) {
        this.totalGaji = totalGaji;
    }
}
