package comp.week2day6.fix2.view;

import comp.week2day6.fix2.controller.StaffController;

import java.util.Scanner;

public class StaffApp {


    public static void main(String args[]) throws Exception {
        //membuat object dari controller
        StaffController sc = new StaffController();

        Scanner input = new Scanner(System.in);
        int pil=0;

        do {
            System.out.println("MENU");
            System.out.println("0. Buat Dummy Data");
            System.out.println("1. Input Data Staff");
            System.out.println("2. Hitung Total Gaji Staff");
            System.out.println("3. Create File Data Staff");

            System.out.println("99. EXIT");

            System.out.print("Input nomor : ");
            pil = input.nextInt();

            switch (pil) {
                // performs addition between numbers
                case 1:
                    //memanggil method membuat file input
                    sc.readerFile();
                    break;
                case 2:
                    //memanggil method untuk menampilkan data staff
                    sc.viewStaff();
                    break;

                case 0:
                    //memanggil method data dummy
                    sc.createDummy();
                    break;
            }
            System.out.println();

        } while(pil != 99);

        // closing the scanner object
        input.close();

    }

}
