package comp.week2day6.fix.controller;

import comp.week2day6.fix.model.Staff;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class StaffController {
    //mendeklarasikan object arraylist
    ArrayList<Staff> arrStaff = new ArrayList<>();

    Scanner input = new Scanner(System.in);

    //method dummy data
    public void createDummy(){
        System.out.println("Creating Dummy Data");
        this.arrStaff.add(new Staff(2,"PETER",3000));
        this.arrStaff.add(new Staff(1,"JOHN",2000));
    }
    //method perhitungan total gaji staff
    public void hitungGaji(){
        int i = 0;
        for(Staff st:this.arrStaff){
            int tunjMakan = st.getAbsensi()*20000;
            int tunjTransport = st.getAbsensi()*50000;

            int totalGaji = (tunjMakan+tunjTransport) + st.getGapok();

            this.arrStaff.get(i).setTunjMakan(tunjMakan);
            this.arrStaff.get(i).setTunjTransport(tunjTransport);
            this.arrStaff.get(i).setTotalGaji(totalGaji);
            i++;
        }
        System.out.println("Success");
    }

    //membuat method untuk object create file
    public void addLoct() throws IOException {
        //menginput data lokasi penyimpanan dan nama file
        System.out.print("Masukan lokasi penyimpanan : ");
        String disk = input.next();
        System.out.print("Masukan Nama File : ");
        String nmFile = input.next();
        FileWriter writer = new FileWriter("C:\\"+disk + "\\"+nmFile+".txt");
        BufferedWriter buffer = new BufferedWriter(writer);
        //melakukan sorting by id
        Collections.sort(this.arrStaff, Comparator.comparingLong(Staff::getId));
        //menulis data yang akan di tampilkan di file
        buffer.write("Id Nama Total Gaji\n");
        for (Staff st:this.arrStaff) {
            buffer.write(st.getId()+","+st.getNama()+","+st.getTotalGaji()+"\n");
        }
        buffer.close();
        writer.close();

        System.out.println("Success");
    }
    //method untuk menginput data Staff
    public void addStaff(){
        System.out.print("Input ID : ");
        int id = this.input.nextInt();
        System.out.print("Input Nama : ");
        String nama = this.input.next();
        System.out.print("Input Gaji Pokok : ");
        int gapok = this.input.nextInt();
        //membuat object untuk memasukan data ke arraylist
        Staff st = new Staff(id, nama, gapok);
        this.arrStaff.add(st);

    }
    //membuat array variabel string untuk menampuk data terbilang
    static String[] huruf={"","Satu","Dua","Tiga","Empat","Lima","Enam","Tujuh","Delapan","Sembilan","Sepuluh","Sebelas"};

    //method untuk mentukan terbilang dengan melakukan perbandingan menggunakan if
    public String angkaToTerbilang(Long angka){
        if(angka < 12)
            return huruf[angka.intValue()];
        if(angka >=12 && angka <= 19)
            return huruf[angka.intValue() % 10] + " Belas";
        if(angka >= 20 && angka <= 99)
            return angkaToTerbilang(angka / 10) + " Puluh " + huruf[angka.intValue() % 10];
        if(angka >= 100 && angka <= 199)
            return "Seratus " + angkaToTerbilang(angka % 100);
        if(angka >= 200 && angka <= 999)
            return angkaToTerbilang(angka / 100) + " Ratus " + angkaToTerbilang(angka % 100);
        if(angka >= 1000 && angka <= 1999)
            return "Seribu " + angkaToTerbilang(angka % 1000);
        if(angka >= 2000 && angka <= 999999)
            return angkaToTerbilang(angka / 1000) + " Ribu " + angkaToTerbilang(angka % 1000);
        if(angka >= 1000000 && angka <= 999999999)
            return angkaToTerbilang(angka / 1000000) + " Juta " + angkaToTerbilang(angka % 1000000);
        if(angka >= 1000000000 && angka <= 999999999999L)
            return angkaToTerbilang(angka / 1000000000) + " Milyar " + angkaToTerbilang(angka % 1000000000);
        if(angka >= 1000000000000L && angka <= 999999999999999L)
            return angkaToTerbilang(angka / 1000000000000L) + " Triliun " + angkaToTerbilang(angka % 1000000000000L);
        if(angka >= 1000000000000000L && angka <= 999999999999999999L)
            return angkaToTerbilang(angka / 1000000000000000L) + " Quadrilyun " + angkaToTerbilang(angka % 1000000000000000L);
        return "";
    }

}
