package comp.week2day6.fix.view;

import comp.week2day6.fix.controller.StaffController;

import java.util.Scanner;

public class StaffApp {


    public static void main(String args[]) throws Exception {
        //membuat object dari controller
        StaffController sc = new StaffController();

        Scanner input = new Scanner(System.in);
        int pil=0;
        do {
            System.out.println("MENU");
            System.out.println("0. Buat Dummy Data");
            System.out.println("1. Input Data Staff");
            System.out.println("2. Hitung Total Gaji Staff");
            System.out.println("3. Create File Data Staff");

            System.out.println("99. EXIT");

            System.out.print("Input nomor : ");
            pil = input.nextInt();
            switch (pil) {
                // performs addition between numbers
                case 1:
                    //memanggil method penampahan data staff
                    sc.addStaff();
                    break;
                case 2:
                    //memanggil method perhitungan total gaji staff
                    sc.hitungGaji();
                    break;

                case 3:
                    //memanggil method membuat file output
                    sc.addLoct();
                    break;
                //meanggil method data dummy
                case 0:
                    sc.createDummy();
                    break;
            }
            System.out.println();

        } while(pil != 99);

        // closing the scanner object
        input.close();

    }

}
